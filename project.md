---
title: /project
layout: page
permalink: /project
---
# /project

## GODOT ACCESSIBILITY DEMO
*2021-Current*
A demo in Godot for an accessible game. It's designed so that a game can be built off of it. It is designed with blind, mobility and mental accessibility in mind.

<iframe frameborder="0" src="https://itch.io/embed/1298519?linkback=true&amp;border_width=0&amp;bg_color=000000&amp;fg_color=81bcf9&amp;link_color=5b6afa&amp;border_color=000000" width="550" height="165"><a href="https://punishedfelix.itch.io/godot-movement-accessibility-demo">Godot Movement + Accessibility Template by Punished Félix</a></iframe>

## PHILOSOPHY FANTASY
*2020-Current*
A philosophy themed card game, made in a week for a game jam. Work gradually on it on the side when I have downtime.

<iframe frameborder="0" src="https://itch.io/embed/785774?bg_color=000000&amp;fg_color=767676&amp;link_color=373737&amp;border_color=333333" width="552" height="167"><a href="https://punishedfelix.itch.io/what-is-philosophy">What IS Philosophy, Anyways? by Punished Félix</a></iframe>

## THE SEQUENCE
*2020-2020*

A JRPG fused with a Braille Tutor! Input the Braille keys when unleashing tablet attacks to deal extra damage or boost your effects! Currently suspended development for now but has a functional demo.

<iframe src="https://itch.io/embed/703701?bg_color=000000&amp;fg_color=ffffff&amp;link_color=222222&amp;border_color=222222" width="552" height="167" frameborder="0"><a href="https://punishedfelix.itch.io/the-sequence">The Sequence by Punished Félix</a></iframe>

## BRAILLEMON
*2013-2016*

Braillemon was my first attempt at making a dedicated game project and was also my first main experiment in blind accessibility. I worked with the members of [audiogames.net][audiogames] to develop the accessibility features. It grew to be one of the most popular audio games in the mid 2010s. It was built in Game Maker Studio.

Braillemon's accessibility model is primative and based around the limited movement. However it is a fully fledged tranlation of the Red/Blue games with some unique custom events.

The project was never completed and was discontinued in 2016. There are two versions - a stable version that ends at Fuschia city, and an unstable version that includes most of the game but is very buggy.

links: [[stable][bmon-stable] | [unstable][bmon-unstable]]

## COLORS CHROMATICS/SPECTRUM
*2016-2020*

Production on Colors Chromatics shortly before discontinuing Braillemon. The idea for the game had lingered for some time but motivated by the confusing storytelling choices of Undertale and various Pokemon fan projects being C&D'd at the time, I decided it was time to begin work on Colors Chromatics.

Colors Chromatics has a much more sophisticated accessibility model, especially in its version. Colors calculates audio cues dynamically based on collision data and allows for free navigation. It also features a sophisticated label system that allows rapid development of accessibility in the game.

links: [[gm demo][colors-gamejolt]]

[audiogames]: https://audiogames.net/
[bmon-stable]: downloads/bmon_stable.zip
[bmon-unstable]: downloads/bmon_unstable.zip
[colors-gamejolt]: https://gamejolt.com/games/ColorsChromatics/248621