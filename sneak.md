---
title: /sneak
layout: page
permalink: /sneak
---

# /sneak

These are instructions on how to install and use [Sneak](https://gitlab.com/Forthegy/sneak), an open-source program that lets you encrypt/decrypt files on the command line. This program was not developed by me, but rather [Forthegy](https://gitlab.com/Forthegy) and they should be [contacted directly](mailto:turing.was.gay@gmail.com) if you have questions (I am hosting it on my site to help distribute it, and helping write documentation).

**Table of Contents:**

* [Introduction](#introduction)
* [Installation](#installation)
	* [Windows](#windows)
	* [Linux](#linux)
		* [Installing Python 3](#installing-python-3)
		* [Installing Sneak](#installing-sneak)
* [Usage](#usage)
	* [Public/Private Key Generation](#publicprivate-key-generation)
	* [Encrypting/Decrypting Text Files](#encryptingdecrypting-text-files)
* [Extended Usage](#extended-usage)
	* [Purge Keys](#purge-keys)
	* [Show Public Key](#show-public-key)
	* [Import Keys](#import-keys)
	* [Encrypt Files](#encrypt-files)
	* [Decrypt Files](#decrypt-files)
	* [Additional Help](#additional-help)
* [Frequently Asked Questions](#frequently-asked-questions)
    * [What are the Private and Public keys?](#what-are-the-private-and-public-keys)
    * [Why should I use sneak instead of GPG/PGP?](#why-should-i-use-sneak-instead-of-gpgpgp)

# Introduction

This program provides signed asymmetric encryption service for protected communication using the [Curve25519 cipher](https://en.wikipedia.org/wiki/Curve25519). It is intended to make this process easy for users who do not have any experience with end-to-end encrypted communication, and who want a method which is (1) not dependent on an opaque platform and (2) totally transparent and easy to understand.

Every user generates a key pair. To encrypt a message for someone else requires one's own private key and the public key of the intended recipient. For the recipient to decrypt the message requires that they have your public key and their own private key. In this way, the users are protected from impersonation and mistaken identity since a message from someone can only be decoded if it was encrypted using their actual private key, which only they ever have.

DO NOT allow anything or anyone to access your private key for any reason. DO NOT transmit a private key over the network. If for any reason you believe your private key is compromised, so state that it is immediately and as publicly as possible so that people wishing to contact you securely will not mistakenly encrypt any messages using the public key that is associated with your private key.

# Installation

## Windows

Download the installer [here](https://gitlab.com/Forthegy/sneak/-/raw/b94f7230631cb0f0206706ba66988fa249f8a0d9/master-dist/sneak33-3.3-win32.msi). When you complete the installer, sneak33 will be installed.

## Linux

### Installing Python 3

If you don't have Python 3 installed, you should install it. To install Python 3, run the following command:

Ubuntu/Mint:
```
sudo apt install python3
```

(if your distribution isn't on here, look online how to install Python 3.6 or higher).

When Python is installed, make sure the version is at least 3.6:

```
python3 --version
```

### Installing Sneak

To install Sneak, download and extract the repository and run the following command in its base directory:

```
python3 sneak33.py build
```

*Note: If you are having trouble installing Sneak, you can run it in the directory of ``sneak33.py`` by replacing ``sneak33`` with ``python3 sneak33.py`` in all commands.* 

# Usage

## Public/Private Key Generation

You will need a public and private key to send secret messages to your friends. In order to generate these keys, run the following command:

```
sneak33 generate
```

It will output a message, if it was successful, containing the location of your keys.


## Encrypting/Decrypting Text Files

To encrypt/decrypt text files, you must first share public keys (the file ending in ``.pub``) with your friend. 

Place your friend's ``.pub`` file in ``~/.sneak``.

Now that the keys are set up, to encrypt a message for your friend, type the following:

```
# This will encrypt the file data.txt and print the results
sneak33 encrypt [friend_name] data.txt

# This will encrypt the file data.txt to the file out.txt
sneak33 encrypt [friend_name] data.txt out.txt
```

It's a good idea to save the encrypted message to a file so its easy to send over the internet.

When you receive your friend's file, you can decrypt it like so:

```
# This will decrypt the file data.txt
sneak33 decrypt [friend_name] data.txt

# This will decrypt the file ./data.txt to the file out.txt
sneak33 decrypt [friend_name] data.txt out.txt
```

**Remember to never share your private key under any circumstances!!**

# Extended Usage

## Purge Keys

If you need to purge keys for any reason:

```
sneak33 purge [name]
```

It will ask you to confirm the deletion by typing in the name of the key's user. If successful, it will delete the key permanently.

## Show Public Key

If you need to display a certain public key:
```
sneak33 show [name]
```

## Import Keys

If you want to import a key from a file:

```
# Only importing the public key means you can only receive files from this user.
sneak33 import [username] [publickey_file]

# Importing both keys lets you impersonate a user.
sneak33 import [username] [publickey_file] [privatekey_file]
```

## Encrypt Files

If you want to encrypt a text file:

```
# This will encrypt the file data.txt and print the results
sneak33 encrypt [friend_name] data.txt

# This will encrypt the file data.txt to the file out.txt
sneak33 encrypt [friend_name] data.txt out.txt
```

If you want to encrypt a binary file (such as a photo, video or archive):
```
# This will encrypt the file data.bin and print the results
sneak33 encrypt-bin [friend_name] data.bin

# This will encrypt the file data.bin to the file out.bin
sneak33 encrypt-bin [friend_name] data.bin out.bin
```

## Decrypt Files

If you want to decrypt a text file:

```
# This will decrypt the file data.txt
sneak33 decrypt [friend_name] data.txt

# This will decrypt the file ./data.txt to the file out.txt
sneak33 decrypt [friend_name] data.txt out.txt
```

If you want to decrypt a binary file (such as a photo, video or archive):
```
# This will decrypt the file data.bin
sneak33 decrypt-bin [friend_name] data.bin

# This will decrypt the file ./data.bin to the file out.bin
sneak33 decrypt-bin [friend_name] data.bin out.bin
```

## Additional Help

You can view the help for this program by simply typing

```
sneak33 help
```

for even more details. This is also displayed if you have a syntax error.

# Frequently Asked Questions

*Note: this section is a WIP*

## What are the Private and Public keys?

When you create a new key, you create two keys - a public key to share with your friends, and a private key to keep secret on your computer. There are two keys because the use of the private key allows you to "lock" a file so that only the recipient can "unlock" it, through encryption and decryption. 

When you encrypt a file, you use your friend's public key and your private key to encrypt it. When they decrypt the file, they use your public key and their private key to decrypt it. This way, only the intended recipient can understand whats in the file.

That means, if you plan on having two way communications with your friends, you should share your public keys with each other. 

The public key and private key are both linked together in a way where they cannot be predicted from each other and brute forcing the key would take an extremely long time. This is why its very important to never share your private key - if someone has your private key, they can impersonate you! Make sure to ``purge`` your key if this ever happens, and ``generate`` it again.

You can read more about public-key cryptography on [Wikipedia](https://en.wikipedia.org/wiki/Public-key_cryptography).

## Why should I use sneak instead of GPG/PGP?

sneak is designed to be simpler to use, to streamline the interface for end users who are not familiar with using technical security applications. 

Regarding security, sneak uses the [Curve25519](http://cr.yp.to/ecdh/curve25519-20060209.pdf) algorithm, which has no known vulnerabilities. RSA encryption is vulnerable to the [ROBOT Attack](https://web.archive.org/web/20200602040842/https://www.trustzone.com/the-robot-attack-rsa-encryption-is-vulnerable-choose-ecc-in-tls-ssl-certificates-to-ensure-security/) - the NSA and IEEE both stopped trusting RSA in 2005. Some versions of OpenSSL are believed to be compromised so any version of GPG/PGP using them is compromised as well. Sneak is built using [libsodium](https://doc.libsodium.org/) which was built to attempt to [address these security concerns](https://cr.yp.to/highspeed/coolnacl-20120725.pdf).


*[[top](#)]*
