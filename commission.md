---
title: /comm
layout: page
permalink: /comm
---

- [Digital](#digital)
- [Traditional](#traditional)
- [Terms and Conditions](#terms-and-conditions)

# /commission information

If you are interested in commissions, my prices are listed below.

# /digital

**Lineart**
<img src="/images/commission/doublehelix.png" alt="deleuze and guattari in the pose of spyral double helix" width="256"/> 

Digital line art drawing. Can be any color, but does not include shading. Can be made transparent on request.

*Starting at $20.*

**Digital Painting**

<img src="/_fanart/emploeon.png" alt="empoleon painting" width="256"/> <img src="/_fanart/natu.png" alt="empoleon painting" width="256"/>

A transparent digital painting.

*Starting at $30.*

**Animation**

<img src="/images/commission/spinarak.gif" alt="spinarak" width="256"/> <img src="/images/commission/natujump.gif" alt="natu" width="256"/> 

Vector-based animation. *I do not attempt to emulate traditional animation techniques.*

*Starting at $45 per 10 seconds of animation, up to 30 seconds, or $30 for a short <5 second loop. Please quote for longer animations.*

# /traditional

**Drawings**

**Marker-Shaded drawing**
<img src="/images/commission/charizardx_ori.jpg" alt="charizard X" width="256"/> <img src="/images/commission/fairy.jpg" alt="a muse with a lute" width="256"/>

A drawing using markers to shade a monochrome picture.

*Starting at $30.*

<img src="/images/commission/charizardx.png" alt="charizard X" width="256"/> <img src="/images/commission/murkrow.jpg" alt="murkrow" width="256"/>

*Can be colored digitally starting at +$10.* 

**Colored Pencil Drawing**
<img src="/_traditional/bird.png" alt="bird drawing" width="256"/>

A drawing using colored pencils, using acryllic white paint to do highlights. In this photo it would be the drawing of the bird shown here.

*Starting at $40.*

**Paintings**

**8"x8" Painting**
<img src="/_fanart/painting-ho-oh.png" alt="ho-oh painting" width="256"/> <img src="/_fanart/painting-lugia.png" alt="lugia painting" width="256"/>
<img src="/_fanart/painting-porygon.png" alt="porygon painting" width="256"/> <img src="/_fanart/painting-treecko.png" alt="treecko painting" width="256"/>

Stylish pop-art metallic paintings designed so that they can be hanged either at a 45-degree angle or normally. Additionally, multiple paintings can be displayed together on a wall and rearranged for unique home decor. 

*Starting at $90.*

*Please email me for a quote on paintings larger than 8"x8".*

**Shipping:** Buyer must pay the cost of shipping if they would like to have the physical copy of the piece. If the art is not purchased, I will destroy/replace/resell the piece after 30 days, depending on the subject of the work. *Paintings require shipment of the physical piece because of the difficulty of reusing canvases.*

# /terms and conditions

**Markups**

Markups include:
* Additional Characters
* Overly complex characters
* Backgrounds
* Panels/Comics
* Mixed Media
* Additional corrections

These will be added as part of your quote and explained.

**Exceptions**

I will not do the following:

* Overly complex mechas
* Extreme fetish content
* Animal abuse
* Anything to do with children

I have the right to refuse any commission at any time.

**Rights**

You are free to use the art commissioned in any non-commercial usage. I retain the rights to the images and source files produced. Please quote me if you are interested in commercial usage.

**Prodecure and Payment**

Please email <a href="mailto:labadore1844@gmail.com">labadore1844@gmail.com</a> with your request. I currently will accept PayPal and Cashapp for payment. *I will not accept a commission without sufficient information about your character and the desired end product*, such as reference sheets, detailed descriptions, and other things. I check my inbox several times a week, so please hang tight; I will respond to all commission emails within a few days of receipt.

I will provide up to one correction per stage of work (sketch, lineart, coloring). Further corrections will require additional payment, depending on the size/complexity of the piece.
