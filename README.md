Do you want to set up your own Jekyll static-generated site like this one? 

To create a Jekyll static-generated website like this one, you will need the following:

* Git
* Ruby
* A Gitlab account

Follow the instructions for your operating system here:

* [Windows](tut_Windows.md)
* [Linux](tut_Linux.md)
