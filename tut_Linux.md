---
title: /tutorial
layout: page
permalink: /tutorial/linux
---
# Set up git
You can install git by following the instructions [here](https://git-scm.com/download/linux).

# Set up Ruby
[ [Detailed Instructions](https://www.ruby-lang.org/en/documentation/installation/)] 
## Installing Ruby
Follow the instructions on [this](https://www.ruby-lang.org/en/documentation/installation/) page based on your distribution.

To test the installation, run this in the terminal:

```
ruby -v
```

If installed correctly, it will read the version number installed.

## Installing Jekyll and Dependencies
Follow the instructions for [Ubuntu](https://jekyllrb.com/docs/installation/ubuntu/) or [Other distributions](https://jekyllrb.com/docs/installation/other-linux/).

