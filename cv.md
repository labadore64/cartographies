---
title: /cv
layout: cv
permalink: /cv
---

## Projects

### PunishedFelix

#### January 2021-Present

##### [Website](https://punishedfelix.com)

<div class="content">
PunishedFelix is a multimedia project that includes this blog, a YouTube channel and multiple social media accounts that involves using cartoons of Félix Guattari to help explain his theories and political ideologies easier through the internet. The goal of the project is to encourage repetition of the figures of his face produced by the cartoon character, as well as finding new lines of flight by "jacking" into the semiotics of memes, video game culture, and other highly online and virtual social environments.
</div>

### Guattari-Wiki

#### November 2024-Present

##### [Website](https://guattari.wiki)

<div class="content">
A website using Mediawiki software to archive, educate and explain Félix Guattari's biographical details, theoretical ideas and other things relating to Guattari. This wiki only allows invite-only users to edit the content and aims to eventually archive texts as well, and currently supports English and French, although only French in boilerplate. Currently a prototype, but feel free to look around.
</div>

### Multiple Indie Game Projects

#### March 2016-Present, on and off

##### [Website](https://punishedfelix.itch.io/)

<div class="content">
My game projects after Braillemon was an attempt to learn how to make visual games with blind accessible components, with varying degrees of success, and further experimenting with using the interface as a storytelling device. I made multiple unreleased games in this time, especially between 2016-2018 and 2022-2024. 

Currently I am thinking about starting another title but I need to scope out my options for the work, since I am currently working on multiple projects.
</div>

<iframe frameborder="0" src="https://itch.io/embed/1634484?linkback=true&amp;border_width=5&amp;bg_color=000000&amp;fg_color=ffffff&amp;link_color=FFFFFF&amp;border_color=FFFFFF" width="560" height="175">Critter Sanctuary by Punished Felix</a></iframe>

<iframe frameborder="0" src="https://itch.io/embed/785774?linkback=true&amp;border_width=5&amp;bg_color=000000&amp;fg_color=ffffff&amp;link_color=FFFFFF&amp;border_color=FFFFFF" width="560" height="175">Philosophy Fantasy by Punished Felix</a></iframe>

### Braillemon

#### 2013-2015

<div class="content">
My first serious attempt at a video game - a copy of Pokémon Red and Blue from scratch, but with accessibility for visually impaired people. Worked with users on audiogames.net to better understand their needs and desires as players. The game was a hot mess and designed to have barely any graphics from the start. It was a really exciting project because it got quite popular for a while in the mid-2010s in a time before video game accessibility became somewhat more widely spread.
</div>

## Assets

### godot-screenreader

#### December 2024-Present

##### [Github](https://github.com/badgernested/godot-screenreader)

<div class="content">
A screenreader for Godot 4.3+. Has a few additional accessibility features, like high contrast mode and VTT parser.
</div>

<iframe frameborder="0" src="https://itch.io/embed/3209244?linkback=true&amp;border_width=5&amp;bg_color=000000&amp;fg_color=ffffff&amp;link_color=FFFFFF&amp;border_color=FFFFFF" width="560" height="175"><a href="https://punishedfelix.itch.io/godot-screenreader">godot screenreader - for games by Punished Felix</a></iframe>

## Writing

### Punishedfelix.com

#### February 2020-Present

<div class="content">
This site is the site where I post all of the transcripts to my video essays. This format can be either enjoyed in a cinematic video, or as an essay piece. Most of the writing is about Félix Guattari, the consequences of the Science Wars/online atheism, disability, video game development and sometimes media analysis. 
</div>

## Professional Experience

### Software Engineer

#### Agillence

##### March 2022-June 2024

* Java developer maintaining full stack Java-based web application and providing technical support for legacy logistics software.
* Reverse-engineered large parts of the code base and third-party software to fulfill development team demands and communicated with the rest of the company the technical functions of the software, including new, unfamiliar technologies. Reported and documented multiple security concerns and potential performance and other code improvements. Helped clear the team’s extensive backlog of tickets.
* Worked with repository management through Git and Bitbucket. Migrated old Java libraries, constructed and maintained SQL reports and built interfaces using GWT, JSP, HTML and CSS. Worked with JavaScript libraries such as HighCharts. Readily learned new technologies like Flutter Android Development.
* Constructed new processes to improve QA and development procedures. Helped improve communications between the development team and the users.
* Readily learned new technologies such as Flutter and reconstructed Twilio’s API.

### Software Developer 1

#### Blue Care Network

##### July 2018-December 2019

* Worked as a Java developer in Linux to maintain and produce batch applications and provided IT support.
* Worked with repository and dependency management tools Maven and Nexus.
* Worked extensively with SQL databases and Hibernate to validate data retrieval in applications and built/migrated SQL routines.
* Helped migrate and upgrade servers from Red Hat 6.0 to 7.0. Collaborated with other developers to ensure the original deadline was met despite numerous difficulties. Educated new developers on local systems to facilitate improved efficiency at all levels within the team.
* Created, upgraded and fixed web components used internally by the company using technologies that included various Java backends, Spring, Hibernate, Jquery, CSS, and REST/SOAP APIs.

### Junior Developer, BPM Consultant

#### IBM

##### September 2016-October 2017

* Built several UIs in IBM Business Process Manager that displayed data transported through IBM’s business processes using JavaScript to control  field visibility and validation in a single form to manage over 50 different process types.
* Worked closely with the client in an AGILE environment to ensure all client needs were met satisfactorily.

### Apprentice Developer

#### Secure-24

##### January 2014-March 2016

* Learned how to build various applications and access rights for Service-Now software.
* Built applications using Javascript applications and used various Javascript libraries such as High-Charts.
