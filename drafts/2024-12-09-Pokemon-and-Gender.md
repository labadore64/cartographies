---
layout: post
title:  "Pokémon and Gender"
date:   2024-12-01 01:44:05 -0500
tags: gender game theory
---

# Introduction

"Are you a boy? Or are you a girl?"

This quote from Professor Oak at the beginning of the Pokémon games, as commonly known by most people. This phrase has been popular on the internet for over 15 years now[^1], representing different waves of attitudes towards gender. At one time, jokes about "checking" what someone's gender was based on "what was in their pants", with a creepy uncle vibe, was commonplace in an online culture where the harassment of women and homosexuals were a common joke. Over time, it transitioned towards a phrase of gender questioning, adopted as an in-joke by many transgender people online, demonstrating the confusion that older people may have with understanding their gender expression. In a way, this simple expression was able to represent a larger question surrounding gender - whether it be one that explores the possibilities of what it can be, to those that aim to reinforce strict its binary - through the physical interface of the machine of the Gameboy playing the cartridge.

![](/images/pokemongender/oak.png)

However, what a lot of people don't realize about this phrase is that the Professor doesn't actually ask you this question when the world first played the Pokémon games in the late 1990s. For the player, gender was not a choice in Generation 1, and it was heavily implied through the writing of these games that you are a boy. It was not until Pokémon Crystal Version, released internationally in 2001, that we had the ability to not only be something besides a boy, but even have the privilege to *choose* our gender, all through the confines of the interface. Not only this, but gender could be expressed through different means as well, such as the Pokémon themselves, the characterization of NPCs, and later in the series, even the selection of clothing available to players. Pokémon was designed as a gender neutral product, as evident by the history of Pikachu's design - the game's mascot - and how it was chosen over the pink and more feminine Clefairy. Despite this effort, the social machines of gender construction in the modern world emerge through these games and their interface design, narrative choices and aesthetics, and it is through this relation to gender constructed through the very interface of the game and its larger interaction as a social subject that produced new relations to gender through Pokémon.

# Assigned Male

- The first pokemon game you are assigned male
- there is no question about whether or not you are male, and you have no choice
    - There are story elements that focus on you being male, such as Erika's gym reacting to you being a boy
    - Defaulting to a male character reinforces the idea that being male is the standard or default sex. reinforced by later games having BOY be the first choice in selecting gender.
    - Discuss being forced to play as a male character as a girl
    
- Despite not having pokemon gender, the first games have a few instances of gendering Pokemon:
    - One trainer asks if male or female pokemon are stronger.
    - Several individual pokemon are referenced to be either male or female.
    - The French version of Pokemon Red/Blue in the Pokemon Mansion journals says that Mew has given birth, but asks if Mew is a "mom" and assumes initially that Mew is male, but possibly female because it gave birth.


Gen 2:
- Gen 2 introduces Pokemon gender, which forces all species into a position into a gender binary. The only genderless Pokemon at this time are machines, and the only new genderless Pokemon is Porygon2, Unown and Legendary pokemon.
    - According to a sun and moon guidebook, genderless pokemon have no gender because scientists cannot determine their gender - which reinforces the idea that traditional biological ontologies place gender determination within the authority of agents of the gendered state such as scientists, researchers etc
        - Might be a good idea to return to Professor Oak's question about gender with Crystal here.
    - Legendary pokemon/Unown being non-gendered - explore this idea more
    - Species like Chansey/Blissey exhibit gender stereotypes through appearance (pink, gentle) and function (healing, kind, giving, egg=mother), Miltank and Kangaskhan represent motherhood, with Miltak having such emphasis on nipples that creepy art in Japan was made for Moomoo Milk of a sentret suckling on her nipples.
        - Strangely, Nidoran-F is the only one in its evolutionary line that can breed :<
    - Male-only Pokemon also repeat gender stereotypes. Tauros is extremely aggressive and is the counterpart to Milktank. Tyrogue and the Hitmons are male, reflecting potentially sexist tradition in martial arts?
- Gender in Gen 2 was determined by a hidden stat, called an Attack IV, which is randomly generated when a pokemon is generated. Depending on the gender ratio of the species would determine the cutoff point for this stat. This stat is used to calculate the attack stat, and the higher the value, the better the stat. IVs were implemented to add random variation to Pokemon. To allow for consistency between cross-compatibility with Gen 1, they could not use newly generated random values. As a consequence, female pokemon in Gen 2 are always weaker than male Pokemon with attack.
- Pokemon breeding further reinforces the relationship between heteronormativity and reproduction, by creating a mechanism where pokemon with compatible personalities can breed, but only under the condition that they share the same gender (with the exception of Ditto, who can become anything). When same sexed partners are stored in the daycare, they don't merely not breed, but they outright ignore each other - reinforcing heteronormative expectations
    - The huge exception to this is Ditto, who is sometimes portrayed as transgender or a prostitute, because of its ambiguous gender and ability to breed with anything. Ditto transforms into a copy of its target. In battles, this includes gender, implying that all reproduction with Ditto is homosexual - a way that homosexuality seeps through the cracks of hard coded relations through intersection of UI and narrative.
- Gen 2 transforms the interface in Pokemon Crystal to allow for the player to not be assigned male, but to choose their gender, which contrasts from real life. This, like many games, gives people an opportunity to experiment with their identity in simulated environments. Because male players can choose to play female, and female characters can choose to play as a male without being forced to, this enables new modes of gender exploration - but the difference between the position of the sexes produced different results.
- The Love Ball - intented to reinforce heteronormativity, through associating "love" with opposite sex. but because of a programming error, it is actually homosexual. This shows how queerness and homosexuality can emerge through minor disruptions to constructed ontologies.
- "Gender" is a censorship of using "sex" to separate gender identity from sexual implications because Pokemon is a kids game.

Gen 3:
- Azurill FTM gender swap. Azurill has a 75% female 25% male ratio and its evolutions have 50-50. This means that 25% of azurill change gender upon evolution.
- Separation of gender from stats, using a randomly generated "Personality Value" that determines gender, among other things, such as shininess, Wurmple evolution, spinda's spots etc. transforming gender from something based on observed social relations between 

Gen 4:

Future gens:
- Transgender trainer in Gen 6. In Japanese it is implied that a black belt (male class) transitioned to a beauty (female class) thanks to medical science. The medical science part was censored in most international releases, and in German/Korean, the transgender implication was censored entirely

Notes with Sabo:

- Stats and Breeding, changes with stats and breeding across generations, how stats and breeding are communicated, relations to aborescent models of evolutionary psychology possibly
- Sabo points out that there is almost never a multiplier for boosting pokemon who often fight together
- this may be because of requirements of the competitive design of the game, which would make the boost basically equal for everyone if implemented fairly (max values, as seen with DVs in gen 1-2)
- Pokemon appear to only have bonds with the player, not with other entities - except with regards to heterosexual pokemon breeding

- Relation between Silver and his dad Giovanni introduced in HG/SS
- Gen 1 has intense oedipalization while gen 2 breaks off. (Does giovanni being absent allow us to ask are you a boy or are you a girl?)

- Gen 1 is about how a kid can do whatever he wants to become a pokemon master. The society of pokemon produces the conditions where a person can emerge who, through a homosexual rivalry with his nemesis Gary/Blue, is able to become incredibly powerful, so powerful that it interferes with a situation of national security
- Silph Co. is a corporation that produces everything that produces the fabric of the Pokemon world, such as pokeballs, potions, etc.. As a result they have central control over the production of the units of pokemon society. When Mewtwo was released, they were commissioned to make a ball that would never break. (did they have the masterball before mewtwo?)
- Gen 2 is more territorialized - more about the intersection between the character ad the player. in regards to gender, this is through the binary gender at the beginning of the game (rather than anything being assigned male) and the breeding system

- breeding not only implies birth and sex but also the existence of death
- Gen 1 - pokemon used for wars, gen 2 - slowpoke being slaughtered for tails
- innocence being broken through ruptures in subjectivity, ironically the poor programming of the game leads to multiple examples of this
- it transformed from an open world to a standardized game
- for many of the games, becoming a woman is just a matter of changing the sprites. but what does that do for the player?
- why is it not okay to eat slowpoke tails but okay to use slowpoke to fight
- its because these things are coded, there is no real explanation for anything, its just repressed, which impacts my world view and introduces nihilism into my life
- (copied) But yeah, while one acknowledges and defines, one also draws limits for what is possible. The state is as much an internal demarcation as a seperation from the outwards. NB would be a war-machine trying to express itself in any way it could in gen 2, be it through genderless pokemons (by making them out to be anomolies) or the pretty androgynous looks of the rival
- Silver probably was intentionally designed to look very androgynous because gender is a focus in gen 2, especially by crystal version
- Gen 1 is early childhood, gen 2 is adolescence
- Bugs in gen 1 were scary, bugs in gen 2 could be used without crashing the whole world
- possibly explore vegan takes
- powers of horror by kristeva - abjection theory
- In a way, a game is as much an interface as any other chain of signifiers are, so any break or abjection raised through games would probably also be viewed as repulsive
- how strange it is to have it so aggressively coded in your face that you have to be asked to be a boy or a girl... who does that
- the alternative of course, seen in gen 1, is comfortable to cis men, but for everyone else, the question becomes a lot more complicated
- Mew and other legendaries cannot breed with ditto or any other pokemon, they were created as part of the arceus myth (maybe use this as a hook for part 2 about radio and pokemon)
- (copied) And the whole reversal biblical story, mew being girl coded and pink, cutesey and mewtwo being this stupid disruptive trauma-bitch male
mewtwo coming from the rib of mew.. or well, pokedex says mew was pregnant so thats pretty inconsistent

- Pokemon Movies and Gender

- Takeshi Shudo, the lead writer of the Pokemon anime from 1997-2002 and the writer of the first 3 Pokemon movies, was given free reign over the first two Pokemon movies because the first movie was produced when Nintendo executives were too concerned about the consequences of the Porygon Soldier seizure situation, and because of the first movie's incredible success, was allowed to have free reign over the sequel.
- Shudo's work, when having the freedom to express himself, expresses deep themes of existentialism, relations to gender, relations to the world at large. He did not want to just make children's stories but stories that engaged children through their own existential questions (something that I've grown to greatly appreciate)
- Pokemon: The First Movie:


- Pokemon 2000:
    - Lugia was created by Shudo, the only Pokemon created by the anime, as a unique character in Pokemon 2000. He did not anticipate the creature to enter the main games, so it was quite surprising
    - The original script did not include team rocket or Ash's group.
    - The name of the movie was "Lugia's Explosive Birth" in Japanese, but it was renamed to Pokemon 2000: The Power of One. This shifts the subject of the film's title from 1) the interaction of territories produced by the legendary birds and the emergence of Lugia, 2) Lugia's connections to femininity and reproduction, and moves it towards the subject of Ash Ketchum, the character of the prophecy. This centers the prophecy not around its own narrative and its own constructs, of which Ash is a part, but rather transforms him into the "Main Character to save the day".
    - (Copy pasted) Shudo says that Lugia’s name was selected by vote at a big meeting. However, Lugia doesn’t have any meaning in Japanese, so it’s unclear why the name was selected. The most convincing explanation of Lugia’s derivation I found comes from Lugia’s Bulbapedia page, which supposes the name might come from the Latin word “Lugeo,” which means to lie dormant, alluding to the way Lugia lies at the bottom of the sea.
    - (Copy pasted) Takeshi Shudo: For me, the most difficult part was actually that last three-quarters. Typically, when characters become aware of the existence of their selves and realize the differences between them and others, it usually leads to conflict. They split. Lugia’s Explosive Birth runs contrary to what’s typical, showing how you can acknowledge these differences and coexist with each other. “You are you. Other people are other people. It’s obvious there are differences between the two, but if you think this way, and approach other people this way, you can avoid conflict.” You could explain it to a child in such simple terms. But adults always overthink things, and tend to make everything unnecessarily philosophical.
    - To write various characters in the film, Shudo would "become" the character, to understand their motives. Simulating positioning himself in the narrative of different characters. His becomings included moving his body like them, talking like them, etc. He mentions that people were worried he had developed Multiple Personality Disorder (DID)
    - Shudo designed Lugia to have female-male sides intertwining, but was dubbed as a male voice, which erased the femininity and consolidated Lugia into a binarized gender. Furthermore, as Shudo progresses, the erasure of femininity becomes more and more apparent and he stresses the feminine identity of Lugia in his blog posts.
    - With a feminine Lugia in mind, the plot can be reframed as a chauvanistic male who believes that feminine territories belong to him, manipulates world currents (moltres, zapdos and articuno) to gain access and possession of a woman, tied intimately with the culture of an island nation. Additionally he can represent the compression of queer identity into a feminine binary to be exploited, extracted
    - The theme of balance is expressed through the territories of the legendary birds, who are organized into territories surrounding the islands. When Moltres is captured, this results in worldwide climate change as a change of these territories. Zapdos explains that since Moltres no longer occupies the Fire Island, it is trying to capture as much territory as possible, which is producing the imbalance. Fire and Ice are constantly creating and destroying each other while Lightning acts as a stabilizing current.
    - While the title "The Power of One" suggests that Ash is the "key" to solving the puzzle of the ancient prophecy, in the film, it is apparent that Ash and his friends interpret him as the one who will fulfill the prophecy essentially on a whim. Lugia does not confirm or deny that he is the person of the prophecy, only telling him to pursue the goal to complete it if he really believes he can. As a result, Ash fulfilling the prophecy is not because of anything special about Ash, but because the narrative calls for someone to occupy a narrative role. He becomes integrated into a culturally-encoded ecological machine by allowing himself to become the subject of the chosen one.
    
- Pokemon The Mystery of the Unown
    

- Shudo died in 2009, likely as a consequence of his drug addiction and alcoholism

- 

[^1]:[Know Your Meme, Are You a Boy? Or Are You a Girl?](https://knowyourmeme.com/memes/are-you-a-boy-or-are-you-a-girl)
[^2]:[Pokémon Crystal Game Script](https://gamefaqs.gamespot.com/gbc/198308-pokemon-gold-version/faqs/49457)
[^4][Bulbapedia - Gender](https://bulbapedia.bulbagarden.net/wiki/Gender)
[^5]:[Pokemon’s Head Writer: Booze, Pills, and Ending Ash’s Story](https://web.archive.org/web/20241231081628/https://lavacutcontent.com/takeshi-shudo-ending-pokemon/) [(Original Link)](https://lavacutcontent.com/takeshi-shudo-ending-pokemon/)
