---
layout: post
title:  "Pokémon and Existentialism"
date:   2024-12-01 01:44:05 -0500
tags: game theory
---

# Introduction

## Mewtwo, Gender and Existence

## Lugia, the Feminine Beast of the Deep

## Entei's Oedipus Complex and the Decoding Flows of Unown

## 

## Summmary

[^1]:[Know Your Meme, Are You a Boy? Or Are You a Girl?](https://knowyourmeme.com/memes/are-you-a-boy-or-are-you-a-girl)
[^2]:(Pokémon Crystal Game Script)[https://gamefaqs.gamespot.com/gbc/198308-pokemon-gold-version/faqs/49457]
[^3]: Félix Guattari, "The Three Ecologies", pg. 36
[^4]: [Bulbapedia - Gender](https://bulbapedia.bulbagarden.net/wiki/Gender)
[^5]: Félix Guattari, "Soft Subversions", pg. 149
