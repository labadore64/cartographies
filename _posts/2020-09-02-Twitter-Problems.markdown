---
layout: post
title:  "Twitter Problems"
date:   2020-09-02 17:25:05 -0500
tags: personal trans
---

First, I would like to apologize for not writing to my blog more recently. After receiving a head injury last year, a lot of my problems got much more difficult to manage, which is why I'm currently unable to work, and try to concentrate most of my effort in creating the demo for [The Sequence](https://punishedfelix.itch.io/the-sequence). I do have ideas for blog posts but they haven't been able to materialize into a more fully fledged product.

The main update for this post is to let people know why I'm struggling so much with suspensions on Twitter. After releasing my initial critique of Nyx Land, I have been harassed, stalked and report abused repeatedly by her and her followers. This has made my paranoia nearly unmanageable and has made the website extremely hostile to me. I have had to block hundreds of users on the suspicion that they may be part of this spy ring, most likely operating from her Discord server, and I'm certain I'm still missing a few. This is all very frustrating because none of these people have even read the original blog post, which while critical of Nyx's abusive behavior, makes it very clear that this has nothing to do with her being a trans lesbian and everything to do with her paper being poorly researched, poorly written and poorly conceived. Because I threatened her clout and status, and because I made her look like a total ass, she is coming after me with psychopathic fury unrelenting.

# Twitter Abuse

My experience with interacting with Twitter is that because of my niche interest in disability, gender and technology, I suddenly become somewhat popular in a Deleuze community, only to be surrounded by people who could give less of a shit about what he wrote and care more about spin off accelerationist esoteric fascist narratives. They invaded my discord server and caused arguments and abused other members, requiring a full clear out of all gender accelerationists from the server. I respond with my apparently obvious and open-ended perspective from an FTM and was immediately attacked, framed to be like a Rowling-esque TERF.

This is primarily because I would point out that transfemme people still have latent misogyny from the toxic masculinity they endured while growing up, and this misogyny expresses itself by treating transmen as cis women and actively oppressing them. I was quite abrasive in my language ("penis-people") but this was to make a point - some forms of misogyny are based around the body, and not gender identity - and the very true reality of "penis-people" defined a lot of the oppression that intersex people face as well (they are literally forced to be penis-people). This is largely because this lapse in MTF narratives makes them extremely easy for TERFs to target them, and is 100% fixable by having a better understanding of feminism beyond a Tumblr-level understanding of gender. I work directly with many MTF friends to make sure that my narrative is inclusive of their issues, because of the sensitivity of this problem - many times of which I'm ignorant of until they highlight them to me. I also include people who are intersex in the discussion, because they are often a target of trans abuse/neglect as well in theory discussion. Despite my best efforts to be honest and fair, this fight continued to escalate.

I must admit I have somewhat of a "nice" personality flaw that is being particularly exploited here - I've dealt with many abusive aggressive people in the past and I generally try to give them the benefit of the doubt and while they may have pissed me off and their actions were cruel, I try to take their point in isolation and transform it into something constructive. Nyx's abuse made me discover a lot of very important things about gender which I would write on my Twitter, and because Nyx is so incredibly narcissistic she simply assumes that this process is about me trying to decimate her further instead of trying to transform a negative situation into a positive one.

Even as I have withdrawn almost entirely from the discussion outside of a snide comment every few days, Nyx continually escalates the situation by subtweeting me almost every day on her account, riling anger against me, even though I had little to do with her most recent drama in the last two months. Indeed, it was Bill Kezos who managed to get her banned on her old account by pointing out her rampant racism! Because so many people are stalking my account, every tweet I make about my own gender struggle is rapidly shared as a cruel meme in their circles (most of which I'm unaware of because I don't check her twitter unless someone has sent me something particularly bad). They try to spread rumors in their side of the community that I am transphobic, that I'm a TERF, that I'm anti-transfemme, which all of which can easily be dismissed by reading my trans writings on this blog. She has tried to destroy my reputation in the Twitter Deleuze community by spreading these lies. She deliberately frames all of her opposition as part of a "Guattari Gang" despite the fact that most of the opposition in the last two months has actually been based around several users who called out her shit after me, and I have been too physically exhausted to spend more than a couple hours a day in front of a computer in the last month or so.

<iframe width="560" height="315" src="https://www.youtube.com/embed/9bYWu0lcNGI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# Theory Abuse

Another major abuse that frustrates me is how many of her followers have been trying to frame famous writers such as Guattari as supporting their views. For example, to try to defame me, this quote was taken out of context from a short essay titled *Becoming-Woman*<sup>[1]</sup>

```
Becoming-woman can play this intermediary role, this role as
mediator vis-a.-vis other sexed becomings, because it is not too far
removed from the binarism of phallic power. In order to understand
the homosexual, we tell ourselves that it is sort of "like a woman."
And a number of homosexuals themselves join in this somewhat
normalizing game. The pair feminine/passive, masculine/active
therefore remains a point of reference made obligatory by power in
order to permit it to situate, localize, territorialize, control intensities
of desire. Outside of this exclusive bi-pole, no salvation: or else it's
the plunge into the nonsensical, to the prison, to the asylum, to
psychoanalysis, etc. Deviance, various forms of marginalism are
themselves coded to work as safety valves. Women, in short, are the
only official trustee of a becoming-sexed body. A man who detaches 
himself from the phallic types inherent in all power formations will
enter such a becoming-woman according to diverse possible
modalities. It is only on this condition, moreover, that he will be able
to become animal, cosmos, letter, color, music. 
```

It sounds like from this quote that Guattari is saying that the female form is the predicted movement for gender, which allows one to give rise to these kinds of esoteric gender-fascist narratives, and would be pretty damning to me if it was left in isolation. What this person didn't include though was the very next paragraph:

```
Homosexuality, by the very nature of things, cannot be dissociated
from a becoming-woman-even non-Oedipal, nonpersonological
homosexuality. The same holds true for infantile sexuality, psychotic
sexuality, poetic sexuality (for instance: the coincidence, in Allen
Ginsberg's work, of a fundamental poetic mutation together with a sexual
mutation). In a more general way, every "dissident" organization of
libido must therefore be directly linked to a becoming-feminine body,
as an escape route from the repressive socius, as a possible access to a
"minimum" of sexed becoming, and as the last buoy vis-a.-vis the established
order. I emphasize this last point because the becomingfeminine body shouldn't
be thought of as belonging to the woman category found in the couple, the
family, etc. Such a category only exists in a specific social field that
defines it. There is no such thing as woman per se, no maternal pole,
no eterhal feminine ... The man/woman opposition serves as a foundation
to the social order, before class and caste conflicts intervene. Conversely,
whatever shatters norms, whatever breaks from the established order, is
related to homosexuality or a becoming-animal or a becoming-woman, etc.
Every semiotization in rupture implies a sexualization in rupture. Thus,
to my mind, we shouldn't ask which writers are homosexual, but rather,
what it is about a great writer-even if he is in fact heterosexual-that
is homosexual. 
```

Also worth pointing out is how often Guattari, who wrote these essays mostly through the 1980's, is being used to dismiss things like nonbinary gender, which were so obscure that Guattari would have been unlikely to know or interact with it much back then. However, what he's describing in this second paragraph clearly suggests destroying the gender binary and using "woman" as a direction of how this binary unfolds. Furthermore, he makes it clear that homosexuality is a form of becoming-woman, which is constructed around being "like a woman". Reading the rest of the essay, he is not suggesting a "feminization" of gender but rather deterritorializing along a feminine axis - which includes all queer people - and means that gender is actually becoming nonbinary through unfolding across femininity. On top of this - he even says that this axis isn't truly real, but socially contextualized, and there are many other becomings it is related to. He identifies the change based on a transformation of the underlying social structure as a sexual movement and this allows him to tie other becomings in a generalized movement of sexualized restructure, allowing us to ask what parts of a writer is connected to homosexuality.

Why not be honest? I don't see Guattari as a prop for my opinions, I see him as a fascinating person who had a lot of really interesting things to say and happens to agree with me a lot - and not even always - but understanding his person the best that I can and the most honestly I can allows me to learn the most from him, I would think. And the truth is, you shouldn't *need* a famous person to tell you that your idea is good enough - the problem is that we artificially value words based on how famous the person saying them is. It's genuinely quite upsetting how malicious this kind of reading of Guattari both because it shows how disinterested people were in the actual real Guattari, as well as showing how little they value their own insights.

# Why Would This Happen?

Well, I'm not stupid, I know what Nyx and her orbiters are doing, they made this painfully obvious when mass reporting my tweets on Tuesday with the "suicide reports" and abuse reports within one hour. We all know why Nyx is mad. It's because I wrote a single tweet that completely demolished her:

![Nyx Land says: I will personally see to it that every genderspecial's delusions of oppression become real. This tweet is deliberately vague so that anyone can interpret this as me invalidating them. To which I reply: Isn't your gender so special you spent 25 pages writing about it](/images/genderspecial.png)

Within a day, she started crying about how I was "invalidating her gender" (how does saying you wrote 25 pages about her gender "invalidate" it? Besides, you're the one invalidating many other people's gender and even gloating about it) and how it was really a shitpost (if that's the case, why does she repeatedly target AFAB and nonbinary people for her vicious attacks?). Someone got hurt where it really hurts - her *ego*. And when that happens with Nyx, screeching banshees will start abusing the report function en-masse.

It's obvious that this was a subtweet against me, despite not even interacting with her or her crew for weeks. Clearly she made a mistake with this one :)

![A suicide message from Twitter saying I can get help.](/images/suicideabuse.png)

This was likely in response to a recent tweet where I was discussing with a professional therapist about how to manage suicide in a clinical setting.

![A twitter suspension message over the following tweet: "I kill black people with this gun" in quotation marks](/images/suspension.png)

This is the only tweet they were able to manage to target since I have been playing it very safe on PunishedFelix. Unfortunately, because of the police brutality, this probably got automatically caught by a bot and I'm now awaiting an appeal. I think I have a decent chance of this being appealed once a human looks at it - it's in response to police brutality and is clearly me mocking racist people, and my account is clearly anti-racist and has no violent threats or aggression. There's a lot to say about Twitter's total dogshit UI and how it easily enables abuse like this, and how it is impossible for the site to improve, but that's a discussion for another time. The bottom line is - she is abusing the Twitter reporting system to try to silence me and anyone else who is actively aware of her shit and active attempts to silence me. 

# Why Is This Such A Problem?

Unlike Nyx, I am currently unemployed and unlikely to be able to find work, to the point that I am actively seeking disability. To try to compensate, I am trying to build a video game called The Sequence, as well as actively participate in Guattari communities and try to build upon his work in meaningful ways to try to influence the accessibility and UX industry at large. Because of this, I have been trying to use PunishedFelix as a means to both make jokes and relate to other Deleuze fans, but also promote my projects and get people interested in disability theory. Because disability has such severe problems with PR already, it is practically necessary for people interested in participating to actively participate. Twitter is the social media platform with the most potential to spread and influence these ideas, so to have my account repeatedly taken down by a mob of angry white racist fascist trans-Karens is beyond frustrating and directly interfering with my ability to receive income. I know that Nyx likes to complain about how poor she is, but she can easily just e-beg her audience of loyal followers for bitcoins, insofar as I am aware, she has never faced the reality of having to submit all of her independence to someone else because of poverty. I don't have this option and I'm very lucky to have access to the few donators that I have to help keep me afloat, but I am still entirely dependent on other people and am disabled.

So, she is interfering with my job, my ability to perform activism, and the promotion of my game, because she is too cowardly to admit she made a poorly written paper. I'm currently re-evaluating my promotion plan, because while I loved being very close to the people who were interacting with my game, and me and others have so much fun with my "Guattari Larper" spiel, this is directly threatening my ability to make future income because of one person's spiteful ego trip.

If I'm unable to retrieve my PunishedFelix account back after this mess, we can all remember PunishedFelix as having made her be so brutally owned that she had to go out of her way to make her cronies target and take down my account in a hilariously obvious and transparent abuse of the Twitter report system. Accounts can be suspended but cringe lasts forever, and nothing is truly erased off the internet.

# Conclusion

At the end of the day, the reasonable thing for Nyx to do would be to correct her paper. She talks so frequently about "castrating the phallus", so might I make a suggestion? Why not castrate the Black Paper Phallus and use it as an opportunity to revise what she's written? Every writer makes mistakes. It's okay to let one paper die to make another even better. Even if her behavior has been vile, disgusting, petty, racist and pathetic, her perspective still matters and she still has the ability to contribute meaningfully if she simply responds to criticisms with honesty instead of relying so heavily on manipulating her uninformed audience. If this really isn't just about clout and ego, then surely she can make a few corrections, right?

Surely she can take more time to elaborate on the contributions of women and their interactions with the computer industry and social apparatus than just a few mentions quoting Sadie Plant? Surely she can reference essays written on the subject that aren't almost 30 years old? Surely she can do a bit more research beyond just assuming open source defeated the military-industrial complex instead of arming them with free software, right? Surely she can read her actual sources and realize that they aren't even saying what she thinks they're saying, right? And this is only the tip of the iceberg of the problems with her work. The entire paper is extremely difficult to read for any technology professional with a few years of experience - especially other transwomen. Clearly this work about gender and technocapital is not targeting the transwomen who actually understands. And its certainly not educational to people who want to learn how gender works in a technologically evolving world, because it presents a complete falsehood of the current situation. It isolates many trans readers who are nonbinary or transmasculine because they are completely alienated from the narrative and she repeatedly targets, harasses and mocks them on her social media - what does she hope to achieve with her over arching manifesto if she just wants to shut up most of the trans community?

It's difficult to even regard it as a piece of art. I recognize that other people have other standards for art than I do, and I'm not expecting every piece to appeal to me, or that works that appeal to me should be regarded as more important. However, I really have to question her motives of this being an expressive piece of trans literature when she makes so many mistakes that completely counteract her trans narrative, especially as she props up Stallman for several pages at the beginning, making the first few pages nearly impenetrable to people involved in the open source community, and confuses the reader with conflicting imagery and prose. Asking for a revision and review after an intense criticism is completely reasonable, especially considering the main underlying struggle she is trying to depict is lost in these aesthetically conflicting choices. This is not a matter of criticizing a work for being too abstract or strange, but rather for a work trying to posit itself as a transfeminine manifesto that starts off with worshiping one of the most openly misogynistic people in computer science and someone who repeatedly allows transphobia to fly within his own organization.

The fact that this work is even regarded as a serious point of discussion in much of the post-left communities is confusing and expresses a serious problem with the quality control of the material being shared. What other things are people constantly misinformed about that repeatedly pop up in the community? Being unaware of the current situation regarding economics, technology, gender and queer studies, communications, theory and many more issues lead to serious consequences for the usefulness of the work of everyone within these communities, and will just lead people down circling black holes of isolated schizoid theories that contribute directly to the social alienation caused by the continued development of capital. This indicates that many people involved may not know how to apply their ideas to a context beyond just a theoretical one, alienating their theories further and further by not allowing connections to resolve these kinds of conflicts.

Again, I'm not saying that we should control what should or shouldn't be read in a community, or even how we should interact with material, but rather suggesting to people to try to interact with their theory in a material context that they can understand and interact with, and using their theory to help communicate these contexts together and produce a meta-theory that is navigated by learning about many people's writings and incorporating your own perspective, instead of centralizing theory around several writers. I've noticed several other writers also have poor theoretical contributions, and understanding the context where these contributions exist within a sociological and psychoanalytical perspective helps improve our own understanding of the world, even if we perceive the work as total garbage. Otherwise, we just open ourselves up to fascist cycles of thinking that prioritize theoretical abstractions while ignoring the actual material problems most of us are trying to resolve with these questions.

It really seems that Nyx wants to just see the conclusion she wants and draw all her evidence to go there. The problem with this method is that she doesn't know the whole world, and so her conclusion will always have major discrepancies with the evidence, and can easily be picked apart. The reason why she are so threatened by a disabled person who barely interacts with her is because they just happened to show the whole world how stupid her essay really is.

I still see the value in Guattari and other related works, so I plan to continue my research. I might take a break from Twitter for a while, if my account is not restored after a month I will try to make an account separate for my game.

Annotations:
[1] - *Becoming-Woman*, Chaosophy, Texts and Interviews - Felix Guattari
