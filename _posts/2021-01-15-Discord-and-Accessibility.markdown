---
layout: post
title:  "Discord and Accessibility"
date:   2021-01-15 12:52:05 -0500
tags: tech needs
---

# Discord and Accessibility

So today, I want to talk a bit more in detail why discord accessibility is a serious problem.

# Why You Should Care

To get this out of the way, some people might be thinking, why should I care about accessibility? Accessibility is for disabled people. I'm not disabled.

First, consider the following. You know how sometimes a game releases a bug that only impacts a very small part of its userbase, but might completely break the gaming experience for them? Many times, a company will try to fix these game breaking bugs so everyone can have a fair chance at playing the game and a random set of programming circumstances they couldn't have prevented doesn't get in the way of their ability to play. In a way, accessibility problems are a similar type of bug, that impact the user interface, especially in ways that may have been hard to predict. However, we act like these bugs are somehow not as important because they are on the user interface side, and often make excuses for why a user shouldn't be able to use said software.

Furthermore, consider how accessibility interacts with the general user interface. Sometimes, people want to be able to use software through a keyboard or mouse only, or want to use Text-to-Speech to read things, or need to change the color themes to high contrast to read in specific conditions. Accessibility actually improves UI capabilities across the board by giving multiple input modes to ALL users, which allows you to just do more with a program.

Also, consider how many communities use Discord. Almost all online promotion and platforms assume that the main discussion platform is a set of social media, which Discord occupies. For disabled people, this is an active barrier to entry for discussion, however because of the strength of the ties throughout the program, its difficult to leave. Even me, as a disabled person, where some of the functionality of discord can actually trigger my migraines and incapacitate me all day if I'm not careful, finds it almost impossible to currently abandon, because of the social power Discord holds over its users (in a similar vein to Facebook with older people).

# Discord's Accessibility Reputation

Since its launch in 2015, Discord has consistently given nearly 0 shits for accessibility. They have a history of ignoring basic requests for features, which are available baseline in many other chat applications, only implementing the groundwork for blind accessibility months before FCC guidelines required gaming communications to implement accessibility. That's not even to say those other apps don't have accessibility issues, because they absolutely do, but pretty much no one goes in so deep as Discord with the commitment to not being committed to edge user cases. Even today, they are still making considerable mistakes that should have been resolved long before release. I want to give an extensive criticism of basic accessibility features. Do not consider this an extensive list of accessibility failures, instead I encourage viewers to talk to other disabled users to talk about the many problems with discord's interface.

First, as I pointed out in a video on my channel, the customization options are slim at best. High contrast is replaced with "dark sidebar", for example. This is not an acceptable replacement. All "dark sidebar" does is give you one additional UI change in only light mode that makes the list of users contrast with the text. Look at the Windows 10 High Contrast mode for comparison - there are major UI element changes that help make features easier to see, and there are multiple options for contrast modes. Older versions of Windows allowed for full color customization of the UI. Are the CSS templates for the UI so spaghettified that adding a simple customization script is impossible for Discord?

Why can't Discord allow its users to have basic customization over its interfaces? All that's available are some font settings, such as padding, scaling and zoom. Allow the standard look as a default, and then allow users to change the color and sizes/fonts of the elements as they deem necessary. If this were possible, major mishaps like this would never have happened.

<img src="/images/discord-accessibility/dyslexia.png" alt="User complaining a new update released in february 2020 was unusable for dyslexics." />

One of the worst features of Discord that still exists in the current version is the "shaky cam" effect it has when working with certain interfaces. This effect shakes the application rapidly for a few seconds when it tries to warn you about certain issues. This features has triggered migraines so bad that I have been incapacitated all day. Imagine literally being knocked out from being able to function for a WHOLE DAY because some stupid tech hipster asshole thought this cool shaky cam effect would look cute in their chat application. That's what it feels like to be disabled and have to use discord.

Discord also has a tendency to completely replace features for no reason. When discord implemented the "reply" feature, it caused the conversation flow of discord chats to be completely disrupted, making it extremely hard for me to follow at a glance who or what was talking, instead of the much more recognized "quote" feature which functioned similarly to established online BBS forums. While this feature isn't a requirement in chat applications, its important to not change the UI suddenly to remove important features and replace them with a new control flow suddenly because of how disruptive this is to a wide range of neurodivergent people and many other disabled people.

[As this article points out](https://blindjournalist.wordpress.com/2020/07/05/discords-accessibility-breadcrumbs/), which is a big recommended read for me and I'll be providing in the description, accessibility has never been a concern for Discord staff. Discord has had a history of alienating its disabled users by refusing to implement popularly demanded accessibility features, ranging from screen reader accessibility to better customization options. As the article also points out, Discord has never been transparent about its accessibility team or efforts and instead relies on invasive detection mechanisms.

Personally, I lost many of my blind connections when I migrated to Discord because the software was completely unusable for blind users until 2020. Skype at the time was too much of a hassle to keep up with, and my account got hacked into and I eventually lost contact. I am still emotional and upset about this because I probably have lost most of those contacts forever. It also makes managing teams difficult because I can't easily coordinate teams between blind and sighted people, so I have to coordinate efforts on multiple sites. However, I also cannot leave Discord because of its influence on social media and promotion circles.

The shadiness of Discord's UI and accessibility is likely linked to its questionable security record, where it has a history of reaping huge amounts of user data and conversations. Discord also readily bans third party clients, which are often a last resort for accessibility for other inaccessible applications, using a frequent update schedule to try to detect these clients more than actually implement basic features and utilities.

While this article suggests that alternatives should be used, I don't think this is viable for many reasons, most notably because of how much market share Discord dominates for the video game market. If you are trying to promote a project, setting up a Discord server is an absolute must, and most games have their own. There is no real alternative because of the close ties that discord has established across the social media machine.

# Wumpus Machine

But lets talk about the thing Discord really doesn't want me to talk about. Wumpus.

Discord's UI is extremely invasively trying to encourage the manufacture of its own "discord culture", centered around wumpus, nitro and server boosting, along with its own advertising propaganda in the form of its "hype squad". Every aspect of its UI is designed to encourage to absorb free energy towards its center, driving a distributed information capture device.

The concentration of Discord's actual UI features are focused around gaming and social interaction, both of which are either paywalled, such as the ability to use external emojis, server customization and upload size, or encouraged, through free content subscriptions. Many would argue that a premium account for customizable features is acceptable means of monetization of many platforms, but in the case of discord, these features are concentrated in a way that focuses on the cultural interactions online. Features like upload limitations make it harder for artists, musicians and other creators to share their work. However, much more insidious is the manufacture of the Discord subculture.

As part of its marketing machine, instead of just encouraging a promotional word-of-mouth system, it manufactured specific internal social territories for these fan marketers. Inspired by Pokemon Go, Discord released the "Hype Squad" - a group of privileged users who are excited about the product and act as word-of-mouth promoters. Instead of being simply one fan community, they split the community into three "houses" - Brave, Brilliance and Balance.

```
"The universe needs people to lead the charge with confident optimism and tenacity.
Without the brave, the HypeSquad would descend into chaos. It takes patience and
discipline to become a vital member of the universe. Without brilliance, the
HypeSquad would descend into chaos. Harmony and poise are necessary to create
equilibrium in the universe. Without balance, the HypeSquad would descend
into chaos."
```

The three groups are effectively interchangeable with no difference in their insides. Indeed, their individualized slogans are remarkably similar, with a mysterious lore that allows users to fill them  with desire. As more and more users interested in the hype perks engaged in the system, the three groups had friendly conflict with one another encouraged through internal competition - acting as propellers to lift the hype machine into the sky. It really didn't matter how the users interacted with it, and the interactions would be filled in to push it forward - From a user interface and social perspective, the design was truly dark and genius.

Now with the mass propagation of Discord's marketing complete, it attempted several monetization schemes, several of which which convince me that Discord's motions are completely unconscious. The most successful were those which returned to controlling the user interface. Most notably, Nitro Boosting is a relatively recent feature which allows communities to improve the settings of individual servers by purchasing boosts. It costs almost $150 a month for a server to receive these benefits for 30 boosts. Additionally, a more expensive version of Nitro exists that distributes 2 boosts per subscription, encouraging the spread of their subscription model further.

By translating the individual user model towards servers, they further weave a culture of consumption that is pushed upon them directly through the integrated user interface that leads directly to a mass data mining operation. While Discord itself is not necessarily directly profitable, its massive reach and ability to encourage commitment to the platform as well as control user flows on a mass scale through boosted servers both offers a massive advertising platform for large corporations as well as demonstrating use case models for user control and dark design.

Discord puts the distribution of this UI far before any usability. Discord notably moved the mobile button layout around to change the image icon with the gift icon, which is also right next to the textbox, making it far more likely that end users will see the pricing screen. Further recall that Discord is used as a primary chat platform for many users and that mass conversion is difficult because of strong social ties and the migration of our social spaces into private territories. Discord and many other popular forms of social media dangle this in front of our eyes to push us further and further into cages where more and more of our interactions can be captured by these global media machines. Additionally, through these designs, it rejects many users from even engaging, thus alienating them from what remains of the communication of most people - a major problem for disabled people who already struggle to receive social interaction.

# Conclusion

It cannot be stated enough times how Discord has demonstrated a reputation of dark manipulative tactics that takes advantage of the highly privatized and viral nature of our social networks. I encourage the use of other smaller networks, both for general accessibility and for a better experience, but again, it's hard to even exist in the modern online world without a presence on these privately owned communication platforms.
