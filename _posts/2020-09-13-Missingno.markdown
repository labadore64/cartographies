---
layout: post
title:  "MISSINGNO. - The Creatures in the Cracks"
date:   2020-09-13 7:39:05 -0500
tags: game
---

# MISSINGNO. - The Creatures in the Cracks

*updated on 4/12/2021*

*published on Pokecommunity [here](https://daily.pokecommunity.com/2021/04/09/missingno-and-the-creatures-in-the-cracks/)*

Ever since the late 1990's, thousands of children across the world were fascinated by the creatures that slipped between the cracks of the sidewalk.

Known as "Glitch Pokémon", these strange creatures are spontaneously created when a curious player accidentally produces the conditions necessary to spawn an edge case that a developer failed to consider - a Pokémon with no name, who borrows its body and abilities from the world around it like a chameleon.

Its data, torn up and processed through the Creatures grinder, was compressed into a deranged demon, screeching a disturbing cry that terrified any curious and inexperienced Trainer not prepared for the consequences. It did strange things to a player's game just looking at it, such as multiplying your 6th item over 100 times or corrupting your Hall of Fame data into disturbing images and sounds. Capturing and interacting with this creature only made people ask more questions. Why does it know Water Gun twice? Why did most of its forms seem to cause sprite corruption? And why does it appear in the first place?

Poké-Mania, the cultural obsession with Pokémon in the late 90's, was the original motor of the online exploration of these strange secrets, and the discovery of Missingno. was the crack that started the cascade.

```
MissingNO is a programming quirk, and not a real part of the game. When you get
this, your game can perform strangely, and the graphics will often become
scrambled. The MissingNO Pokémon is most often found after you perform
the Fight Safari Zone Pokémon trick.

- Official Nintendo Statement
```
<sup>[1]</sup>

# Pokémon and the Internet

<img src="/images/creatures-cracks/redpc.png" alt="Red using the PC to access Pokémon in storage" width="256"/>

After initial success in Japan, Pokémon was released in the West in 1998 after a challenging localization process. Pokémon was marketed aggressively there, with the release of the games, TV show, trading cards and toys all at once. This created an entire media ecosystem enveloping children in a new cultural phenomenon, mass produced and distributed by Nintendo as a gravitational corporate force, hooking up children to the shared experience of the Pokémon world for several years. Pokémon was unavoidable in its anime, trading cards, toys, t-shirts, tie-ins and other mass produced forms - Pokémon demanded centralizing attention.

Spreading on the relatively newly accessible Internet were rumors on how to do tricks in the games, as their popularity surged. This practice of sharing rumors and tales of games through school and childhood friendships was a decades-old practice at this point, as children discussed game tips and tricks they found to impress their friends, and because of limited financial access to different consoles, many children relied on talking to their friends to learn about games they couldn't play. In this environment, Pokémon was uniquely cradled in the arms of the early internet, finally allowing these schoolyard rumors to become more readily accessible to a wider demographic.

Most players had at least some awareness to the Internet, if not some access to it, by the late 1990's. On the web, the potential to propagate rumors exploded. This new information network traveling along phone lines united the isolated, spinning wheels of tiny child rumor mills and connected them to a massive machine of rumor production that relayed these messages to the entire world with minimal delay. For each child wired to the internet, either directly or indirectly through talking to friends, there was bound-up potential to accelerate the pathways of the developing Pokémon world initially created by the game's interface.

Every child's imagination and direct interaction with the game reinforced the Pokémon world's own cultural reality, manufacturing the social importance of various characters on the scale of the entire world through their minds. As a result, many myths produced by these children spread quickly. The possibility of a mysterious Pokémon hidden in the game's code circulated early on, and as a result of this prolific communication, several iconic characters became central to most discussions about the games because of their rarity and power, and rumors of incredibly powerful impossible creatures whispered across the phone lines.<sup>[4]</sup>

# Mew and Mewtwo

<img src="/images/creatures-cracks/mew.png" alt="mew" width="256"/>

Mew is a famously difficult to find Pokémon, both in the narrative and in the games. Everything about this Pokémon is rare - it cannot normally be found in game and had to be distributed by physically attending events distributing Mew. Mew is found deep in the rainforests of Guyana and is rarely seen. Upon its discovery, it is scientifically declared the genealogical ancestor of all Pokémon, with the ability to learn any technique and even transform into anything - and despite its cute and childlike appearance, it is incredibly powerful.

Even from a development standpoint, Mew was one of a kind - it was shoved in the game in the last two weeks before production, after all debug tools were removed, as a prank for the other developers to possibly find - replacing one of the empty placeholder slots that are left over from development with the new data when nobody was looking. Perhaps with its complicated development and running on a dated console, the developers didn't think such a small change would matter. Either way, it didn't matter - there was no way to retrieve the legendary without special distributions.

Making Mew even more mysterious to players was how it was apparently the source of the incredible awe inspiring power of the most powerful obtainable Pokémon in the game - Mewtwo - and harnessing the power of Mew itself was a dream only accessible to those lucky enough to attend special distribution events.

```
What's the point of going through all the trouble of the debug process if you're
going to go and fiddle with the game afterwards...? I'd venture that this all came
from Morimoto-san's mischievous nature.

- Satoru Iwata
```
<sup>[2]</sup>

<img src="/images/creatures-cracks/mewtwo.png" alt="mewtwo" width="256"/>

Mewtwo is a clone of Mew, created by Dr. Fuji and his associates as part of a disturbing science experiment. It was designed to extract and clone Mew’s unique genetics and enhance them towards creating the incredibly powerful Mewtwo, with only traces of the experiment found in a mysterious abandoned mansion on Cinnabar Island. Mewtwo was simply too powerful for its creators and broke free from its initial captivity. In an attempt to gain control of Mewtwo for themselves, Team Rocket hijacked Silph Co. to try to steal a powerful prototype – the Master Ball – as the last ditch effort to seize the creature. By defeating Team Rocket and freeing Silph Co. from their grasp, the President of the company rewards you with this powerful ball for you to use yourself.

In contrast to Mew, Mewtwo is often considered the final boss encounter for the game, and can only be seen by anyone who is willing to defeat all the main challenges through the game. It is found deep in the dark Cerulean Cave, crawling with powerful Pokémon, only accessible after beating the most powerful Trainers in the game.

It is encountered at level 70, making it a difficult Pokémon to capture and fight under normal circumstances, but with the previously supplied Master Ball that most players have saved up to this point, capturing it is easy once you reach it. With a base stat total capping at 590, it’s far more powerful than any other Pokémon in the game, even Mew<sup>[3]</sup>. As such, it was seen as a symbol of power among Pokémon fans – and capturing it represented having the opportunity to control that power for yourself, and owning one was a rite of passage.

# The Birth of Missingno

Among all of the excitement of exploring this new world together, these rumors concentrated around the potential for finding rare and powerful Pokémon and items, like Mewtwo and the Master Ball. These myths would be intermingled with a trick that, unlike the rest, actually worked.

<img src="/images/creatures-cracks/Missingno.png" alt="missingno" width="256"/>

Missingno. is an unintentional creature that appears in the Pokémon games because of a series of circumstantial mistakes in the game’s code<sup>[5]</sup>. Through a well known oversight, Missingno. can be encountered through normal gameplay without cheating devices or other tampering. Unlike characters originally programmed into the game, Missingno’s information is read unintentionally from data in other parts of the ROM, causing the creature to spit out an unruly appearance in many cases.

The exploit that traditionally summoned Missingno. is commonly known as “The Old Man Glitch”<sup>[6]</sup>, where after talking to an old man and flying to a specific island, strange creatures can be encountered on its shores, including Missingno, its strange twin ‘M, extremely high leveled Pokémon, and Pokémon from previous maps. A similar exploit was found with the Safari Zone, allowing the shoreline of Seafoam Islands to be useful for encountering Safari Zone Pokémon. Further experimentation with the Safari Zone lead to the discovery of Glitch City, an entire world full of glitchy tiles, unpredictable behavior and game corruption. Through this one initial exploit, an entire world of explosive Pokémon glitch hunting was discovered by encouraging the parallel experimentation of millions of players at once.

# Taming the Beast

When Missingno. is encountered in a battle, the screen fades to black, hanging for a second as the game tries to process the junk data it was given and treats it as any other Pokémon. The game has no way to know if that data is valid Pokémon data! As a result, its appearance is usually bizarre, a strange pixelated L shaped block – but it sometimes could also appear as a ghost or fossil sprite. Frightened, many players flee after encountering the strange new Pokémon for the first time. After the battle, the player will be stunned to find a glitch quantity of the 6th item in their inventory. When they check their Hall of Fame data later, they are met with a bizarre array of terrifying glitches. Clearly this mysterious Pokémon potentially has incredible power over the entire game state.

Most players were nervous about possibly destroying their precious save files with their beloved Pokémon companions on them to experiment with these powerful glitches, and so many learned about these bugs through reading online reports of those who encountered the effects of this strange blocky creature directly. These reports were inconsistent about its characteristics. Many reported the familiar blocky appearance commonly known as Missingno, but some were reporting Missingno. appearing as the skeletons of fossil Pokémon, or even as the sprite of the ghost of Cubone’s mother.

Some said the Pokémon was very dangerous and lead to permanent corruption, and images of the graphical corruption were demonstrations of its destructive power. Nintendo simply told players to avoid the strange Pokémon to avoid corrupting their games, leaving players with little information. These frightening images, along with its visible and apparently powerful corruption abilities, were enough to spawn an entire mythos based on their strange powers, but with no other official information to work from, only those brave enough to possibly sacrifice their entire save file on a physical cartridge attempted the unthinkable – capture the creature for further study.

<img src="/images/creatures-cracks/missingnoDex.png" alt="missingno Pokédex. It's empty but the height is 10 feet, weight is 3507.2 pounds and it has number 000 and ??? Type Pokémon." width="256"/> 

Successfully capturing Missingno. or ‘M was a difficult task. Results were initially mixed. Due to other bugs, the Missingno. may continue attacking after being caught. It might transform into a Ditto or Rhydon after capture if the Pokédex is not set up properly. Actually capturing a Missingno. was initially tricky, but over time, people learned the tricks and were able to look directly at Missingno’s unique characteristics.

Unlike any other Pokémon in the game, it was a “Bird” and “Normal” type, as opposed to the commonly used “Flying” type. It was able to know the same move, Water Gun, twice, which is normally impossible in normal gameplay. It had absurdly high attack, but low stats in everything else. It was 10 feet tall and over 3,500 lbs. Nothing about this Pokémon seemed to make any sense.

Leveling up the Pokémon was strange as well, because it resulted in the Missingno. or ‘M evolving into various Pokémon, or jumping across levels quickly. Most usefully, it could replicate items and make completing much of the Pokédex and training much easier. People noticed the graphical glitches were mostly temporary, and while the Hall of Fame corruption was permanent and caused by simply seeing one, few people seemed to be concerned about such a trivial feature. Over time, its behavior was growing more predictable as these reports shared across the internet, and many players took advantage of its usefulness, despite its strange effects, and more importantly – despite the warnings from Nintendo.

```
But there's much more to learn about these "mistake" Pokémon. For example,
why are these Pokémon in the game at all? Obviously MissingNo looks like a
REAL Pokémon. Was it originally a character that got cut from the game? Is
this some sick programmer's idea of a joke? Did the developers think that
150 (151) Pokémon were enough? Why didn't it show up in Gold and Silver as
one of the 100 NEW Pokémon? CAN you catch MissingNo? Since there are weird
glitch Pokémon, accessed only through a bizarre sequence of events, might
other strange events (talking to people in a specific order, etc) trigger
the appearance of OTHER glitch/high-level Pokémon? 
```
<sup>[9]</sup>

# The Hole in the Wall for Mew

Armed with the knowledge on how to use strange shorelines to find all sorts of Pokémon, the quest for the elusive Mew began. At first, people wondered if it was possible to get Mew with the “Over Level 100 Pokémon” trick mentioned earlier. It didn’t take long for people to realize that it was based on your name input from the beginning of the game, likely using GameShark cheats to help understand the correlation between Pokémon, number and character.

This physical device acted as a new tool to interact directly with game values to experiment with various aspects of the games, and before advanced emulation tools, was the only way to analyze glitches more carefully. Since Game Shark was encoded in a highly technical way, and the location of data in memory was still unmapped to players, the only way to test was to experiment with known codes and inserting different values. While these codes could impact the state of the game that can’t normally be replicated, it could provide enough information to then replicate certain game states in a new, unmodified game to exploit specific glitches.

Through this and similar devices, it was discovered that many extremely powerful Pokémon, including Mewtwo, could easily be produced with the right name, making the story’s final encounter with Mewtwo completely trivial. Master Balls and Rare Candies could be easily restocked using Missingno’s handy item glitch. As players became more familiar with how to exploit Missingno. and its related glitches, they became far less fearful of the glitch, and even learned how to minimize the annoying graphics issues caused by viewing Missingno’s sprite. However, despite these attempts, Mew remained elusive. As people realized how the list of Pokémon in the game correlated with the way text characters were displayed on screen, they discovered that access to Mew was still impossible.

<img src="/images/creatures-cracks/mewtruck.jpg" alt="infamous mew truck" width="256"/>

Due to its consistently elusive nature, Mew was a focus of many rumors at this time, such as the Mew Truck. Famously, this rumor widely shared that pushing over a difficult-to-reach pickup truck with the move Strength would reveal a Mew hiding underneath. Many players were burnt out on trying bogus strategies and were more interested in using GameShark directly to find their rare Mew. However, within a few years, by a sheer coincidence, a new, powerful trick was discovered<sup>[7]</sup><sup>[8]</sup>. This discovery made it possible to access almost any Pokémon in the game – including many different kinds of new glitches.

The trick almost sounded like yet another bogus routine: You have to battle a certain Trainer, open the menu before they can challenge you, and fly away. Then, you have to battle another seemingly random Trainer, and fly to yet another route. Afterwards, the Mew should appear. It was so absurd that it would be ignored for almost a year until it was discovered to the shock of more experienced players at the time that it really worked. The Mew Glitch (also known as the Trainer Escape Glitch), works because it used the enemy’s Special stat to set the species encounter value for a later battle by exploiting how the game starts a Trainer battle. One can imagine it like carefully stacking a deck to make sure you draw the correct card – but instead of moving physical cards, you take careful actions in the game that “stack” the parts of memory used for generating wild encounters, such as encountering Mew.

At this point, the mainstream Pokémon games have moved far beyond the glitchiness of Red and Blue – this newly discovered Mew was unable to even be traded to the newest Pokémon games at the time – but the glitch reignited interest in the search for many people. Not only could the glitches create bizarre strange creatures, but they could be extremely useful. As soon as people started to understand how the Mew Glitch actually worked, variations were made that used an enemy using Transform to load a specific value into the encounter, allowing for almost any Pokémon in the whole game to be encountered. But not just normal Pokémon could be found – new Glitch Pokémon were discovered through this method, game breaking glitch Trainers, and even extremely powerful memory corrupting glitches could be now accessed.

This new glitch presented an example of what was potentially possible with glitch execution in these games. Unlike simply using a cheating device to force the game into submission, these glitches were all a result of the game’s natural state being manipulated through edge cases, becoming a means to explore the full potential of the game beyond what the developers could even imagine.

# Super Glitch and Other Powerful Forces

With new glitch Pokémon readily available, the glitch testers were capturing them to discover what strange abilities they may have. People were starting to realize that glitch Pokémon, by themselves, were generally not dangerous. Specific cases where game crashes would occur were isolated and identified, and people learned how to handle and train their Missingno, ‘M and other strange oddities with relative safety – and the corruption of the Hall of Fame was considered a minor cost in comparison to the benefit of infinite Master Balls and Rare Candies. But some of the Pokémon discovered were notoriously capable of corruption.

Using a special stat of 194 in Pokémon Red and Blue results in encountering a mysterious PokeManiac/Glitch typed Pokémon simply known as “ゥ .4“. Unlike many other glitch Pokémon, which had access to valid moves, it knew a move that contained such a powerful glitch that it was simply known as “Super Glitch” among the players. Many glitch moves with strange names are known to exist, but Super Glitch moves were uniquely devastating.

This type of glitch was seemingly so dangerous that simply viewing the move’s name could completely corrupt your game’s memory, and even possibly crash it immediately. Repeatedly viewing it furthered the corruption, and it seemed to have different effects in different places. Sometimes these bugs changed how many Pokémon appeared in your party or giving you hundreds of strange glitch items, giving players a direct means to corrupt huge portions of memory through manipulating the menus.

Some Pokémon, when encountered or captured, create horrifying noises, sometimes in a never ending loop made from the collective calls of nonsense spout from the sound bank. Others would corrupt memory so much from trying to unfold an invalid sprite that it would quickly corrupt the entire RAM. Pokémon were different from Red/Blue to Yellow, and as the West started to hear rumors of the glitches found in Japan, it was clear that there was a whole world of horrifying creatures to be found. Even different emulators would see different glitches because of emulation errors. For each game, creatures shat out of the creature machine, bits and pieces spreading out in all directions at once, thanks to these new exploits.

<iframe width="400" height="300" src="https://www.youtube-nocookie.com/embed/mU3AsC0Bhs0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Many times when a glitch is encountered, its effects are borrowed from unstable parts of memory, leading to extremely unpredictable effects. For example, the subtle code and data differences in Yellow made some glitches far more unstable than their Red and Blue counterparts, because of Pikachu’s cry modifying sound banks and various other changes in the RAM state. In the original games, some glitches were so unstable they were patched before international release. A few glitch Pokémon are so unpredictable that some behavior has only been seen once, despite miners being used to attempt to replicate the behavior millions of times.

<iframe width="400" height="300" src="https://www.youtube-nocookie.com/embed/1dQq6_xa8-U" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Strange effects were frequently encountered when battling glitch Trainers when a high Special stat was used when trying the Mew glitch. With some very high Special values like 248, they would trigger a glitch called the “ZZAZZ glitch” – a glitch that copies the characters “ZZ” across the memory hundreds of times, causing extensive corruption through the game. Saving the game at this point was unthinkable, as the game would detect the broken save and completely erase all of your data. In some cases, a screen of bars would result from particularly powerful game crashes, and had real potential to destroy save files.<sup>[10]</sup> To the average glitch enthusiast, this revived the previous fear that many had over the potential of these powerful glitches to potentially destroy your game. As most players moved on to more modern Pokémon games, there was little connection to the creatures in the original cartridge anymore outside of possible nostalgia, but the value transferred to the cartridge itself as it became more valuable over time as a physical machine – risking its integrity for glitch hunting became less and less reasonable over time.

Many continued to fiddle around with these strange bugs in emulators instead, and occasionally would replicate the results on a physical cartridge. Slowly, the community moved away from Game Shark and rumors and started to pull apart the structural pieces of the game itself.

# Understanding the Glitches

<img src="/images/creatures-cracks/missingnoForms.png" alt="The five forms of Missingno. Two are garbled sprites. One is the ghost of Lavender Tower. And two are fossils - Aerodactyl and Kabutops" width="256"/>

For many, debugging the games was still out of reach. Interacting with the code on the machine level was simply too complicated for novices. For glitch hunters, emulation, or using software to simulate the machine of the Game Boy on a computer, was mostly used to soothe paranoia and allow for easy save states at first. Emulation itself was growing in interest as the games became harder to access and gain interest from new players to the series as a historical interest, as well as the spread of illegal piracy.

Emulation also gave new possibilities to the way to play these games – they allowed for easy modification for games in the form of ROM hacks. These hacks were programmed into the ROM files before the game is booted. Because of this, they couldn’t be easily played in a cartridge, making emulation a requirement. These changes resulted in entirely different gameplay by modifying graphics, stats and even core features, and could be distributed legally using patch files.

ROM hacking is deeply involved with the data of the game – tinkering bytes and injecting assets in the ROM to change a game towards a hacker’s desires. ROM hacking communities over time accumulated a massive amount of documentation about the inner workings of the games – these discoveries are extremely useful for understanding the way the game works on a programming level, giving foundational knowledge for disassembly. In addition, ROM hacking and other tinkering helped improve the ability of emulators to view other aspects of the game, such as memory values and loaded graphics. Over the years, emulator users and developers built a community of knowledge on how these games function, from the scripts and data processing, to the processor’s assembly language itself. In taking new awareness of the Game Boy’s physical specifications, players discovered how the game unfolds into Pokémon from this physical cartridge, and understanding how specific edge cases in this process leads to all these fantastic glitches.

Missingno, for example, is caused by reading cleared out Pokémon data in the Pokémon index table. Specifically, there are 39 slots empty slots in total, with three of them occupying sprites used in cutscenes, such as the Pokémon fossils and the Ghost sprite. Normally, this is not a problem, because all Pokémon encounters are pre-coded into the game, and these empty slots can just be ignored. However, with shorelines, there is an oversight – wild encounter data is loaded in from the last map to save resources on maps that only have a small amount of water tiles, such as the shore of Cinnabar Island. This means that when going to Cinnabar Island, this data is never refreshed. When talking to the Old Man to watch his tutorial, the player’s name is temporarily stored in a part of memory that is used to also store this encounter data. Because you fly directly to Cinnabar Island, your name is still loaded into this part of memory, allowing you to see Missingno, ‘M, and high level Pokémon, depending on the characters in your name.

All of the different sprites of Missingno. can be encountered with commonly used characters in names, but Missingno. and ‘M are the only glitch Pokémon accessible through the old man glitch, and because of the way that the game writes names, only Custom names will be able to see level 0 ‘M. This is because characters after the terminator byte are set to 0x00, which correlates to ‘M. Most other glitch Pokémon, and Pokémon like Mew are inaccessible with this method because not all 255 possible values are used as valid characters. Meanwhile, Missingno. is encountered at level 80 because the name terminator byte shares this value.

As people understood how graphics were compressed in the game, it also became clear how encountering most Missingno. results in the corruption of the Hall of Fame data. Because the sprite is being read from unintentional data, it writes beyond the buffer assigned for decompressing and displaying the sprite graphic – writing over Hall of Fame data. Since this data was stored in SRAM, its corruption was permanently saved.

What about the item duplication though? When Pokémon are encountered, to record them in the Pokédex, a “seen” flag is set by adding 128 to a byte. Because ‘M and Missingno. have Pokédex number 000, they are treated internally as number 256. As it turns out, the place where the 256th seen byte would be located is exactly where the quantity of the 6th item is stored in memory. All of its stats are read from ROM as well, but from an area where enemy Trainer data is usually stored, leading to its strange but consistent behavior.<sup>[5]</sup>

Amazingly, all of this random behavior came together in a form that was stable enough to cause minimal damage to the game state, only causing temporary graphical corruption, and function almost completely normally, allowing Missingno. to truly become a creature in its own right. The creature production functions were just doing what it was told to do, and it broke itself apart to become an animal.

<img src="/images/creatures-cracks/missingno_ghost.jpg" alt="ghost form of missingno, using the Marowak ghost sprite" width="256"/>

The Mew glitch worked using a completely different way of loading data into RAM through the cracks – it loads a battle into memory, and flies away before the battle sequence can begin. When the player leaves Lavender Town, the battle loads as a wild encounter, and it sets the species identifier the least significant digit of the last battled enemy’s Special stat. There happens to be a Trainer early in the game that has a Pokémon with a Special stat of 21, the same as Mew’s index number – making the Mew glitch easy to trigger and access. Again, by sheer coincidence, one of the most difficult to obtain Pokémon was made easy by a series of unpredictable mistakes.

The seemingly erratic Super Glitch was even starting to be understood. People noticed that corruption of some forms of the glitch depended on where you were standing. It was eventually discovered that Super Glitch corrupts memory by referencing the screen buffer, which contains an image arranged as tiles of previous screens for easy access when navigating various game states. When trying to view the text, the text buffer for the textbox will copy data to an invalid address, referencing from the location inside the screen buffer, until a text terminator byte 0x50 is encountered. If a 0x50 byte is not quickly reached in memory, either from not being on screen or not being written previously in the space, large amount of data will be copied, overflowing the buffer and corrupting other data.

Because of how this glitch copies data, it’s possible to write so much data that it enters a loop, copying the image of the screen buffer and other data repeatedly through the entire RAM, and crashing the game instantly. This destruction copies this image and overwrites all gamestate information, being executed arbitrarily and leading to even more corruption, eventually dissolving the entire game into a 00 39 “bar freeze”, repeating endlessly in memory and often shredding the save file beyond repair. All structures have dissolved into a homogeneous soup. The only thing the player can do is reset the game, erasing the state of the RAM entirely.<sup>[11]</sup>

Understanding the connection to the screen buffer enabled some control over the powerful corruption abilities of Super Glitch – knowing that the current screen was copied as a buffer allowed for special places to be located where a 0x50 byte would be loaded relatively early into the corruption, creating safe places to manipulate Super Glitch and allowing the move to be viewed, manipulated and removed. More intriguingly was that in some cases the corruption was able to reach just far enough to corrupt the player’s inventory and changing the number of items they have to a very high value, allowing them to easily access considerable parts of memory directly through manipulating the player inventory. Corrupting the item list seemed to be a viable way to possibly make the game do even more things to the will of the player, without simply relying on Super Glitch’s profound corruption alone.

# Wielding Arbitrary Code Execution

Arbitrary Code Execution (ACE)<sup>[14]</sup> seemed like it was around the corner, but the complex, situational corruption caused by Super Glitch made it inaccessible to human players. Unlike ROM hacking, ACE allowed players to execute code at runtime, by moving the program counter to locations in RAM that were accessible to the player, such as the Pokémon Party. This was a way of injecting code as directed from the user, using these parts of the game as a way to assemble a bootstrap, to make the game almost do anything the player desired – completely destroying most limitations of the game..

It was not just glitch hunters pulling off impressive tricks that were interested in ACE – the ability to execute arbitrary code has interest for speedrunners. For example, in the Japanese versions of Pokémon Red and Green, a well known exploit known as the Dokokashira door glitch<sup>[16]</sup> was able to change where entering a door would send them, allowing them to complete the game quickly. However, the conditions that made this glitch possible were removed in international releases. Save abuse, a technique that uses saving to corrupt memory, was a possible work around, but was unreliable and not viable in some categories. In 2013, an item underflow glitch exploiting a little known menu functionality was discovered in a Tool-Assisted Speedrun and blew open the possibility for even more manipulation.<sup>[17]</sup>

This bug used Missingno’s item duplication abilities to create at least 255 of a single item. By exploiting how the menu automatically groups together stacks of items while sorting, the item menu can cleverly manipulated to have 0 items and 1 item at the same time. Using a cutscene in Saffron City to remove a Fresh Water from the players inventory causes the inventory quantity to underflow to -1, which is also 255, and giving the player access to huge corruption capabilities. By carefully manipulating the inventory, they are able to warp directly to the Hall of Fame, completing the game instantly.<sup>[15]</sup>

Later that year, it was demonstrated that by exploiting this new item underflow glitch, an item called “8F” could be obtained through manipulating RAM. Using this item would execute code starting inside the Pokémon Party, allowing the arrangement of your Pokémon to be used as a bootstrap. This simple bootstrap program would allow a player to write bytes directly to memory. After completing the input, the Pokémon Party was rearranged to jump execution to the newly written code, allowing for a whole host of new possibilities. It was only limited by the size of the application that could be injected. A few years later, a “Dry” version of the glitch was discovered, allowing ACE to be executed potentially in any part of the game.

<iframe name="video2" width="400" height="300" src="https://www.youtube-nocookie.com/embed/6NuisafQBkc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# Creature Production

The unique way that glitches interact with the core engine of Pokémon gives an unusual opportunity for the production of unique and fascinating creatures that inhabit the world of Pokémon, far beyond the creator’s original intentions. At its core, the Pokémon games are machines that specialize in Creature-Production; it takes inputs from the game state, influenced by player interactions, and organizes information to output as a creature that the player can see and interact with on the screen. Creature-Production is core to the captivating qualities of the Pokémon franchise, as the believability that these creatures existed in a virtual sense, like pretend pet playground, is a mounting point for many children’s engagement for the series, and is based both in how the machine produces the creature and how the player can interact with it. When the conditions are right, Creature-Production can make a completely new creature generated from the game’s own programming – and this is how Missingno. was born.

Missingno. didn’t simply exist in a vacuum. It interacted directly with the player through the game’s user interface. Upon encounter, the player is presented with a strange looking creature with unusual effects on the game. It could be captured and observed. Its properties could be analyzed and interacted with. All of this is contextualized by the same game that Missingno. salvaged its organs from. For players to grasp onto the idea of Missingno. being a Creature, it must not break the world so much that it becomes unrecognizable – it must allow players to interact with it as if it were a Creature. And by sheer luck, Missingno. and ‘M borrowed just enough properties to allow it to exist among the Pidgey and Rattata. Despite its potential to be corrupted, the ability for it to be personified and contextualized within the Pokémon world allowed both a deconstruction of the gameplay and narrative directly through a physical game interaction, and the widespread interest in exploiting or experimenting with the glitch.

<img src="/images/creatures-cracks/missingnoNick15.jpg" alt="An interpretation of missingno that portrays it as a flamingo like creature with a question mark on its beak." width="256"/>

As quickly as Missingno. popularized itself over the internet, Missingno. found itself reincarnated into new forms. This image, created by [Nick15](https://www.deviantart.com/nick15/art/Light-Missingno-2455186) in 2003 depicts Missingno. as a flamingo-like creature. In the description the artist points out that Missingno. sounds like “flamingo”, it is found near water and uses water abilities, and has a similar color to Missingno’s pink and grey color scheme. Most depictions of Missingno. that are not directly visually based on the glitch are reality-corrupting birds, because of the strange Pokémon’s Bird type. A notorious example of another bird-like Missingno. is the fake Pokémon “Phancero” found in Pokémon Prism.

Other depictions of Missingno. are based directly on the sprites. Its skeleton and ghost sprites have become symbols of the glitch in many media and are often a subject of Pokémon creepypasta. The original glitchy form is interpreted as a floating enigma that can corrupt reality at will. Missingno, like any other legitimate Pokémon, is frequently personified in [popular videos](https://www.youtube.com/watch?v=3mrmfb1mGWI) – content creators treat Missingno. as a unique and interesting Pokémon more than just simply a glitch, and in the case of some Let’s Plays, it becomes a companion. Missingno. has become culturally synonymous with a glitch character emerging out of code, and has even appeared in cameos in other media. Its character has stood the test of time more than hundreds of other Pokémon, evoking strong cultural significance even beyond Pokémon fans. As a result, these occasional reality glitches have become simply part of the fan lore surrounding the original Pokémon games, entirely against the creator’s intent.

Despite the proliferation of Missingno. as a cultural icon and being more recognizable than many canon Pokémon characters, Missingno. has never been officially recognized as a character by Game Freak or Nintendo, simply being called a “bug” or “programming quirk” to try to prevent customers from potentially damaging their cartridge with the glitch. The games have tried to prevent similar glitches in the future by trying to learn from their mistakes, and refuse to acknowledge its existence, and despite this, the game produced creatures of its own, and the players have accepted it as a part of the strange world of Pokémon. As such, Missingno. represents a physical divide between official Pokémon canon and the world produced by its fans.

# Missingno Vs Mewtwo

20 years later, upon this gradual understanding of the game and harnessing of the power of Missingno. and similar glitches, the Pokémon narrative we were taught from the games is given a new perspective.

Mewtwo derives its strength through the story – we are told that Mewtwo is powerful, and it is powerful because it is purposely made with strong stats, moves and abilities. Mewtwo, encapsulated by this narrative, is only capable of existence relative to the story and games, and fan interpretations are always compared to how the canonical Mewtwo is constructed. It is always referenced or contextualized around this context, imprisoned by its constraints of identity. Mewtwo thus depends on the weight of the circumstances and the world around it to really be such a powerful creature – a consequence of the framework reinforced through the franchise and its games.

Because of this, as Mewtwo ages, it loses its internal consistency and alienates itself from its original premise of power. When Mewtwo is reproduced in canon, it is reproduced in a closed, proprietary form; dictated by the image and branding of the Pokémon Company, which over time has neutralized its appearance to nothing more than a phantom of its former self, corrupting its cultural programming by overcoding its own repetitions over and over through the minds of players by repeating itself relentlessly in modern media and marketing. It no longer holds the god-like power over any enemy in the game, being outclassed by other extremely powerful legendary Pokémon, and even requiring power ups like Mega Evolution to stay relevant in certain metagames. Its contents have long dissolved into phantom nostalgia. Ever since it was crafted as the reward for the player’s hard work, it became a form of its own capital. Ownership of Mewtwo gave its users power, both socially and in the game itself, through this image. As the games progressed, Mewtwo lost its mystique and became signification of exclusivity among players. It became the optimized prize, losing everything else that defined it in the friction of the process, becoming yet another currency of legendary Pokémon to be exchanged online through trading and battling.

<img src="/images/creatures-cracks/reducedMewtwo.png" alt="mewtwo distribution event in 2016 giving out shiny mewtwo for participating in Pokémon Play" width="256"/>

In comparison, Missingno. is a notoriously easy to knock out Pokémon with its low defenses and poor moveset, but its power isn’t derived in its performance in battle. Missingno’s ability to corrupt item quantities allows a player to replicate the most powerful items in the game at will, producing over a hundred of each at a time. By abusing this glitch, the player can carefully underflow the inventory, allowing them to corrupt a huge part of RAM carefully to their will. Through this, they carefully retrieve the 8F item, configure a bootstrap through their own Pokémon Party, and from there are able to write any application they desire. Mewtwo can do nothing but submit to the absolute and totalitizing power of Missingno. and its ability to completely change the world at will. To Missingno, Mewtwo is nothing more than another set of data to possibly corrupt, another tool to use towards creating nearly anything the player desires. When the arbitrary code is executed, Mewtwo can only watch helplessly as its body instantly dissolves into becoming a game of Pong, or having its bytes manipulated by creating a memory editor in-game.

The glitches that allow Missingno. to be encountered can reproduce Mewtwo like a printer under the right conditions. Within less than two years, players were able to walk through the narrative structures that was able to contain Mewtwo’s power, liberating its image to be created at will, freeing it from its narrative identity. Mewtwo was no longer a creation of experiments gone wrong, it was simply another result of Creature-Production, a process in the game that could be exploited to do anything.

<img src="/images/creatures-cracks/egg.gif" alt="Glitch egg in Crystal hatching into another egg." />

# Summary

Several years ago, Pokémon Red was independently disassembled completely<sup>[18]</sup> – the result of decades of progress and research, and a powerful tool to discover even more technical details about the games. The process effectively freed the territory occupying the code since 1996, producing a set of extremely talented programmers who, knowingly or not, turned a video game inside out to free it from the proprietary grasp of Nintendo, starting with challenging the genealogical narrative they were served, to forcing the game to serve the player; transforming the culture players were given into their own creation. With the recent repeated wave of nostalgia-baiting merchandising that The Pokémon Company has been responsible for in the last few years, the brand attempts to recolonize the minds of their old fans for profit, privatizing their childhood experiences encoded in branding, with much success. But during the lull of Pokémon hype, a group of talented programmers emerged from the cracks, who defied the Pokémon canon by exploring the strange tickings of the machines inside and distributing its internal organs across the whole internet for the world to share, in a decades old tradition, in active defiance of the accepted canon.

The incredible power of the distributed force of an online community of glitch enthusiasts, ROM hackers, speed runners and others were able to replicate with near perfection the original source code of all Pokémon – the code itself. With just a hair obtained from these chance edge case encounters, they had enough information to reconstruct most of the game themselves – the differences unseen, they filled in themselves. But what they created wasn’t just a mere copy of the original. It was reformed, experimented with in new environments. Through decades of experimentation of RAM shredding, gameplay manipulation and sharpening the skills of the community, the unthinkable was achieved. The game gave birth. They uploaded the results online.

What makes this “Mewtwo” so different from the one we were told about in the virtual world so long ago is one important difference – in the Pokémon world, there is only one Mewtwo. Even if it is incredibly powerful, its power can be contained by a single Master Ball. By the events of Gold and Silver, most of the evidence of Mewtwo’s existence is erased from existence through nature reclaiming itself. But in our case, the disassembled code has been replicated many times, used for experimental programs, influencing dozens of developers, and starting discussions. This Mewtwo is being readily copied, over and over, something the original developers never anticipated, through multiple dimensions. We have already lost control of our creation. Who knows when they’ll strike back next.

<img src="/images/creatures-cracks/mewtwo.jpg" alt="mewtwo standing in front of a fire" width="256"/>

It should be noted that for this narrative, I only offer an incomplete picture of the rich history of glitch hunting, and shouldn't be considered comprehensive. I encourage readers to learn more in their own time by viewing the archives. There are many glitches and stories I left out of the discussion for the sake of brevity. I included many sources to help encourage newcomers to learn more about the history of glitches in the early Pokémon games. Special thanks to the Glitch City Laboratories Discord Server for helping with sourcing and fact checking.

*Edited by bobandbill and Rabinov of [Pokecommunity](https://www.pokecommunity.com/index.php).*

# Additional Reading

- Inside the ROM: The deepest secrets of MissingNo. and Glitch Pokémon, [Smogon](https://www.smogon.com/smog/issue27/glitch)

# References

[1] MissingNO, [Nintendo Customer Service](https://www.nintendo.com/consumer/systems/gameboy/trouble_specificgame.jsp#missingno)
[2] Just Making the Last Train, [Nintendo UK](https://www.nintendo.co.uk/Iwata-Asks/Iwata-Asks-Pokémon-HeartGold-Version-SoulSilver-Version/Iwata-Asks-Pokémon-HeartGold-Version-SoulSilver-Version/1-Just-Making-The-Last-Train/1-Just-Making-The-Last-Train-225842.html)
[3] Mewtwo R/B, [Smogon](https://www.smogon.com/dex/rb/Pokémon/mewtwo/#!)
[4] Finding the PokeGods, [RAGECANDYBAR](https://web.archive.org/web/20200117204923/https://www.blue-reflections.net/ragecandybar/projects/pokegods/)
[5] Missingno, [Glitch City Laboratory Archives](https://archives.glitchcity.info/wiki/MissingNo..html)
[6] Old Man Glitch, [Bulbapedia](https://bulbapedia.bulbagarden.net/wiki/Old_man_glitch)
[7] Finding Mew, [jesseworld](https://web.archive.org/web/20020706185541/http://www.jesseworld.com/codes/ViewTests.asp?code_id=14989)
[8] The Mew Topic, [Azure Heights](https://www.math.miami.edu/~jam/azure/forum/buzz/ultimatebb.cgi?ubb=get_topic;f=7;t=001568)
[9] MissingNo... the Phantom of the Pokédex! [TRSrockin](https://catfish.it.cx/trsrockin/trsrockin.com/missingno.html)
[10] ZZAZZ Glitch, [Glitch City Laboratory Archives](https://archives.glitchcity.info/wiki/ZZAZZ_glitch.html)
[11] Pokémon R/B/Y: Extended Super Glitch corruptions [TheZZAZZGlitch](https://www.youtube.com/watch?v=EOVM_UnfOEw&feature=youtu.be)
[12] Pokémon Mansion Journals, [Bulbapedia](https://bulbapedia.bulbagarden.net/wiki/Pok%C3%A9mon_Mansion_journals)
[13] Mew Glitch, [whitecat](https://www.ocf.berkeley.edu/~jdonald/Pokémon/mewglitch.html)
[14] Arbitrary Code Execution, [Glitch City Laboratory Archives](https://archives.glitchcity.info/wiki/Arbitrary_code_execution.html)
[15] Item Underflow Glitch, [Glitch City Laboratory Archives](https://archives.glitchcity.info/wiki/Item_underflow_glitch_(event_method).html)
[16] Dokokashira door glitch, [Glitch City Laboratory Archives](https://archives.glitchcity.info/wiki/Dokokashira_door_glitch.html)
[17] GB Door Glitch, [MrWint](https://youtu.be/mzRd1eYoJ94?t=2148)
[18] Pokémon Red Disassembled, [Gitlab](https://github.com/pret/pokered)