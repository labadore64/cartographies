---
layout: post
title:  "Did \"Fashionable Nonsense\" and the Sokal Affair really destroy Post Modernism?"
date:   2022-01-11 01:44:05 -0500
tags: theory
---

In 1996, Alan Sokal and Jean Bricmont, inspired by the work of Paul R. Gross and Norman Levitt, set out on an academic battle. They read through many works by French theorists and finally engage their attack by producing the infamous prank paper *"Transgressing the Boundaries: Toward a Transformative Hermeneutics of Quantum Gravity"*, also known as the Sokal Paper, submitted to the journal *Social Text* in an attempt to destroy the infamous menace of "Postmodernism". After the ruse was publicly announced, *Social Text* embarrassingly had to redact the article and the event seemingly destroyed the reputation of "Postmodernism", the journal and the theorists mentioned, in an event known as the Sokal Affair. Afterwards, they released their findings of all these writers to the world through their book *Fashionable Nonsense*, published 2 years later.

So, did the Sokal Affair and Fashionable Nonsense DESTROY Postmodernism?

Not really.

# Sokal's book - Fashionable Nonsense

Lets start off with Fashionable Nonsense. It came afterward, but contains the majority of Sokal and Bricmont's actual critiques against postmodernism. Or, rather, writers whom they consider "postmodernist".

To the credit of Sokal and Bricmont, unlike Gross and Levitt, it seems like they actually took some time to attempt to read the source material. At least sometimes, instead of quoting obscure literature or secondary sources, they focus on important, primary texts, with reference to scholars of their works rather than random nobodies, which makes this work immediately much more useful as a critique. And unlike Gross and Levitt, they focus primarily on actually criticizing the authors, even if the criticism barely understands the source material, or mixes up other sources with the writer, instead of injecting petty politics and constant racism and homophobia. Congratulations, the most minimal academic effort was made!

The basic summary of this book is bored scientists and mathematicians criticizing works that they don't understand, with the most elementary of critiques, which often are presented in an extremely dishonest or confusing way. They start off nearly every section with mocking the authors and implying they are nonsensical, which I considered very manipulative and unprofessional - shouldn't we be shown that these are "charlatans" instead of being told so to begin with? This leads readers with the idea that the criticized authors are already guilty as charged, instead of giving them a chance to actually be analyzed.

It's somewhat obvious that these critiques were not a result of actually reading the books, but rather poking around at passages that they thought might find the most material for their own work. Unfortunately, few, if any, of these criticisms actually provide a productive discussion, and rather account for either minor mistakes, ignorance of the material or, most frequently, misinterpretation on Sokal/Bricmont's part.

Perhaps the closest to the most relevant critiques are that of Lacan. They are right to accuse Lacan of misappropriating mathematics to propose his theories. It is true that many people aware of mathematics are turned right off from Lacan's interpretation of it. 

However, the problem with this style of critique is that it is extremely limited in its capacity to critique. What really is the problem with Lacan appropriating mathematics? Is it really because the mathematics is incorrect? After all, the concepts embedded in his mathemes could be simply appropriating the symbols of mathematics - something that occurs in many fields without too much problem. However, to Sokal and Bricmont, there is something fundamentally problematic about combining the realms of psychology and topology.

> "Perhaps the reader is wondering what these different topological objects have to do with the structure of mental disease. Well, so are we; and the rest of Lacan’s text does nothing to clarify the matter."  - Alan Sokal and Jean Bricmont 

They basically assume the reader to agree with them that combining these ideas with psychology is somehow problematic. When Sokal and Bricmont criticize Lacan for 

> "No argument to support his peremptory assertion that the torus “is exactly the structure of the neurotic” [...] Moreover, when asked explicitly whether it is simply an analogy, he denies it."  - Alan Sokal and Jean Bricmont

they are providing no real explanation for how this misappropriation is a problem.

In this excerpt, Lacan is clearly saying that in his theory, neurosis is represented by a torus like structure. Since Lacan believes his theory to represent a real phenomenon, of course the torus would "be exactly the structure of the neurotic". Whether or not this is a problem is not even approached by Sokal or Bricmont - merely the fact comparison exists in of itself! Clearly, this structural component is emphasized somewhere more thorough than an interview.

It should be noted to viewers that mathematics and psychology are not necessarily mutually exclusive terms. While Lacan's usage of mathematics was at times questionable from a scientific modeling perspective, the arguments against his models (for example, his insistence that the neurotic's mind may be modeled as a torus of some kind) seen in this text do nothing to address the incorrectness of this model, and in fact, no good faith effort is made to try to understand what he means by this. Perhaps the points of the torus are mental states, and the hole in the torus has some meaning with respect to accessible mental states. One could even guess that this meaning is most intelligible formally if the torus is interpreted as a differential form, although he was very unhelpful when asked for clarification. While I personally don't understand Lacan's model, dismissal of a model of psychology or psychiatric disorders on the grounds that the model is mathematical alone smacks of ignorance about the history of psychology, and misleads readers about the state of the field and the art.

Since the middle of the 20th century, the application of mathematics to a ground-up model of nervous activity in the brain intended to be the basis of a model of recognition and cognition has been seriously pursued, culminating today in successful and promising family of models which employ specially distributed codes, algebraic topology, and graph theoretical approaches. There is absolutely no reason to assume that this work does not enable us to understand psychology in terms of neurology by way of understanding how the brain functions, and the field of neurocomputation, deriving models from these observations, has produced applicable results in this vein. Indeed, this is the chief motivation for the work aside from improving model imitations of the brain. Meanwhile, Bayesian modelling is applied frequently both in behaviorist approaches and psychiatric outcome prediction methods.

So clearly, math and psychology is not the problem here.

What is needed is a generalized critique of Lacan. Lacan's problem isn't really that he appropriates mathematics. Rather, one critique is about how he constructed his ideas about language and the unconscious - his theory was about structuring these things in a specific way. As stated previously throughout this series, Lacan was already criticized almost 25 years before Sokal released his infamous paper by Lacan's former close student Felix Guattari, who criticized him not on a specific misuse of terms, but rather the idea that language or terms could be used to describe the unconscious at all. Notably, Sokal and Bricmont seem completely oblivious of the fact that Deleuze and Guattari produced such a scathing criticism of Lacan, because it's mentioned nowhere in the book.

Why do none of these scientists seem to understand the basic history between the interactions of these writers? I thought they were concerned about the preservation of facts and truth, but they don't know the basic facts of whom they are trying to criticize. Considering Fashionable Nonsense was published initially in French, this is inexcusable.

## Deleuze and Guattari

The critique of Deleuze and Guattari directly in comparison is much less professional. The pair describe them as

> With a bit of work, one can detect in this paragraph a few meaningful phrases, but the discourse in which they are immersed is utterly meaningless."  - Alan Sokal and Jean Bricmont

But in reality, the majority of the chapter seems to either misunderstand the original text, or is simply crying about how math can't be combined with some other subject - something we've already seen is not true. By the end, there is even a section where they decide to stop giving real examples and instead list off page numbers from their notes.

They complain frequently about words being used out of context, a terrible critique of authors who purposely created new words and used terms from many fields to describe their own ideas. They seem utterly confounded by passages such as this that really don't seem that complicated.

They further embarrass themselves by suggesting that Deleuze is claiming that he has somehow solved classical problems in mathematics raised by Newton and Leibniz. However, they do not understand the nature of Deleuze's words from Difference and Repetition. Deleuze is not merely talking about calculus, but rather the abstract concept of difference, a theme that appears throughout his work - and is comparing the concept to developments in calculus. This is completely different than what Sokal and Bricmont suggest - that Deleuze is trying to somehow trace old ground. By limiting Deleuze's discussion to the kind of difference found in calculus, Sokal and Bricmont frame Deleuze's work in an illegitimate light, which make their critique seem much less honest. 

Having such an arrogant attitude when its obvious that the work is not understood nor taken in an honest context is frustrating to read, to say the least. Of course a complex work that is hundreds of pages long is going to be difficult to understand. This is why fingering through a text in bad faith just to find problems with it is a bad idea. It seems like what they really did was poke through a few pages and say "the math is bad" without trying to understand the context of the work. But any work can become unreadable if taken out of context. How does this prove anything?

They then go on to criticize Felix Guattari's solo work, Chaosmosis. Important to note that this was one of the first works by Guattari ever published in English, recently published at the time and making this work particularly challenging to understand in 1995. They state:

> "This passage contains the most brilliant mélange of scientific, pseudo-scientific, and philosophical jargon that we have ever encountered; only a genius could have written it."  - Alan Sokal and Jean Bricmont

Concerningly, this quote is the same section as Richard Dawkins quotes in his book review of *Fashionable Nonsense*. This is a huge problem when we consider how many times this book review was cited online as if Richard Dawkins had read any of these titles directly, and issued no correction to these citations in almost 25 years.

Here's the second part that Dawkins didn't quote:

> The ontological relativity advocated here is inseparable from an enunciative relativity. Knowledge of a Universe (in an astrophysical or axiological sense) is only possible through the mediation of autopoietic machines. A zone of self-belonging needs to exist somewhere for the coming into cognitive existence of any being or any modality of being. Outside of this machine/Universe coupling, beings only have the pure status of a virtual entity. And it is the same for their enunciative coordinates. The biosphere and mecanosphere, coupled on this planet, focus a point of view of space, time and energy. They trace an angle of the constitution of our galaxy. Outside of this particularised point of view, the rest of the Universe exists (in the sense that we understand existence here-below) only through the virtual existence of other autopoietic machines at the heart of other bio-mecanospheres scattered throughout the cosmos. The relativity of points of view of space, time and energy do not, for all that, absorb the real into the dream. The category of Time dissolves into cosmological reflections on the Big Bang even as the category of irreversibility is affirmed. Residual objectivity is what resists scanning by the infinite variation of points of view constitutable upon it. Imagine an autopoietic entity whose particles are constructed from galaxies. Or, conversely, a cognitivity constituted on the scale of quarks. A different panorama, another ontological consistency. The mecanosphere draws out and actualises configurations which exist amongst an infinity of others in fields of virtuality. Existential machines are at the same level as being in its intrinsic multiplicity. They are not mediated by transcendent signifiers and subsumed by a univocal ontological foundation. They are to themselves their own material of semiotic expression. Existence, as a process of deterritorialisation, is a specific inter-machinic operation which superimposes itself on the promotion of singularised existential intensities. And, I repeat, there is no generalised syntax for these deterritorialisations. Existence is not dialectical, not representable. It is hardly livable! (Guattari 1995, pp. 50–52)

Well, this is still pretty complicated. But most of Guattari's solo work is equally as dense. He fits a lot of content into one sentence!

It's worth pointing out that earlier in this essay, titled "Machinic Heterogenesis", he defines terms like "autopoisis" earlier in the essay, in this case, on page 34 - meaning "auto producing". Out of context, this paragraph seems overwhelming and confusing, but in context, it actually makes a lot of sense. When he talks about ontological relativity, he's referring to how structures ontologically could be different depending on who you ask, and that this is inseparable from enunciative relativity, or how these structures are produced - and he claims that these ontological structures can only emerge from relationships between these self-producing social machines.

He is saying that the relationship between these machines is what produces a context allowing something to exist - that it can't live on its own outside of being a pure virtual component. He uses the cosmos, relative to our planet's interactions, to point out how our context on a biological planet shapes the relationship with our position in the Universe to our own existence, and how the rest of what we understand about cosmology is dependent on other self-producing context machines that we cannot experience directly.

Thus, he transforms our current understanding of the cosmos into a specific set of ontological coordinates, a virtual space that he compares to a dream. He's not saying that these things don't exist or that we live in a dream - but rather that the way we understand and enunciate the universe is based on a virtual system of coordinates that is inherently separate from the abstract machines producing them. And we can see that every time we find out something new - when we do, the system changes and adapts to this new information. Even time itself is produced as a structure - think about how everything from an hour to tiny amounts of time are structured into units - even though the thing that produces time is real, the irreversibility of time is a consequence of the structures that are produced by the communications between the interfaces of these machines.

He describes the possibility of how such structures we normally associate with intelligence could emerge from relationships between things as large as galaxies, or things as small as quarks - a sort of "intelligence" that exists between these structures, normally inaccessible to our plain view. These structures don't undo the power of the machines that produce them, it doesn't make the things that make quarks and galaxies disappear, but rather it gives them a new context and a new way to enunciate these machines, in a way that contains a substantial new amount of possibilities unknown from the perspective of people.

This intelligence, comparable to the intelligence of the economy or states, is actually another ontological universe - another structured set of truths and falses - produced by a new structure. And he stresses that these structures do not follow the rules of language, that they do not follow a universal syntax. He likely stresses this as an anti-chomskian, but it is unclear if he is referring specifically to formal language theory or languages in general.

So, this isn't actually that complicated at all. He's just saying that the structures behind our ontological understanding of things is based on structures that emerge from machines, and that there could be many kinds of these structures all around us that exist between machines in ways we haven't predicted - and that because of this, NO representation of existence really works, and NO existence is dialectical, like language. All existence is produced by these asignifying relationships between machines! 

## WHY I DONT WANT TO READ THE OTHER CRITIQUES IN FASHIONABLE NONSENSE or how I learned how to love reading practically anything else

The rest of the critiques follow a predictable, aggressively repetitive and boring formula: describe basic level knowledge of the writer's background, and list off mathematical facts they get wrong. There is little analysis or even experimental theory - that's to say, unless you want to laugh at literary writers and psychoanalysts being portrayed as not knowing everything in math, there is little value to the critiques presented in the book. They are generally very petty and often reflect a lack of understanding in the writers. I was honestly pretty disappointed, it's like reading a void for 90 minutes. There's only so much that can be said without repeating myself, so I only present a few of their critiques here. 

For example, I don't know much about Julia Kristeva, but the critiques here are flat and vapid. They start to make criticisms on her mistakes in mathematics. I'm not sure whether these mistakes are true or not, I'm not personally very mathematically inclined but more worryingly, they also seem to imply that applying set theory to political theory is a problem, without further analysis of the quote. Instead they use it as an example of her work "invading" the sciences and mathematics by discussing its consequences in other fields, like psychoanalysis.

> "Kristeva has also tried to apply set theory to political philosophy. [...] Kristeva’s mathematical erudition is not limited to set theory. In her article “On the subject in linguistics”, she applies mathematical analysis and topology to psychoanalysis."  - Alan Sokal and Jean Bricmont

Why is this so problematic? Do Sokal and Bricmont believe that mathematics can only be applied to physics and science? Who came up with this rule? Economists and artists of all kinds, especially the rendering arts, are living examples of fields that are not necessarily "scientific" that use and appreciate mathematics. And what about everyday people, at work, at home, who use various kinds of math to do everyday living? There are many everyday people who discover the usefulness of higher mathematics and use it in their everyday lives. Do Sokal and Bricmont believe something offensive has happened here, too? And more importantly, have Sokal and Bricmont not heard of political game theory? 

It appears Sokal and Bricmont's point is - as part of the Fashionable Nonsense formula - is to say that someone's application of math is inherently wrong because there are mathematical mistakes being made. If mistakes are made - so be it! I want those mistakes corrected. But Sokal and Bricmont don't realize that they still haven't completed their primary goal - explaining why it's so problematic to combine these ideas together, and explaining why her conclusions are so wrong.

Irigaray's criticism is just as empty and boring, but they start to play dirty here. They start the chapter with such critiques as pointing out that Nietzsche did not literally know about nuclear physics. Of course he didn't! Irigaray wasn't claiming this. She was making an obvious analogy.

Their critique is so barren that when Sokal and Bricmont criticize her for not understanding that General Relativity has little to do with nuclear power plants, it doesn't occur to them that, in context, her mistake is not so muddled up. In fact, in context, she is largely talking about Einstein and his relationship with his own science. While General Relativity doesn't directly have anything to do with nuclear power plants, Einstein's work in atomic theory and eventually how his work was both used in the extreme violence of the atomic bomb, and the nuclear power plant. It is undeniable that Einstein is related to our relationship with nuclear power.

But two can play at this game: later Sokal/Bricmont insist that

> "The concept of inertia certainly appears in relativity theory, as it does in Newtonian mechanics; but it has nothing to do with human beings".  - Alan Sokal and Jean Bricmont

Does this imply that human beings are not physical objects impacted by inertia? Obviously, people are impacted by the forces of physics. But I don't think anyone was assuming that in the first place. Can you now see why this kind of critique isn't helpful in any way?

Irigaray's chapter gets worse when later, Sokal and Bricmont start to quote N. Katherine Hayles, an American scholar from Duke University, instead of Irigaray, to criticize her work on fluid dynamics. Hayles and Irigaray have fundamentally different points in their works.

Hayles is clearly talking more about a relationship between fluid dynamics and the fluids of sex, and trying to irritate scientists in the field, while Irigaray makes a much more bold statement about the development of fluid dynamics and science as a whole, explaining that the mathematics of the in-between is far less explored and slower to develop than the mathematics of what appears to be there.

You can't criticize one person's work with another person's interpretation! This is just lazy scholarship - which leads to an embarrassing confusion and an entire chapter de-legitimized. Of course someone's quotes thrown out of context with critical information left out is going to be easy to make it sound ridiculous!

But don't take my word on it - Pierre Gilles Lemarié-Rieusset writes in his book The Navier-Stokes Problem in the 21st Century, makes a direct jab at this comparison himself in the introduction to his textbook on the problem:

> In a provocative paper published in 1974, Irigaray muses on fluid mechan- ics with, according to Hayles, “elliptical prose and incendiary reasoning.”  Her thesis has been turned into a kind of “post-modern myth” by the French-Theory bashers Sokal and Bricmont, relayed by Dawkins in his survey in Nature of Sokal and Bricmont’s book Fashionable Nonsense. Dawkins’s irony focused on Hayles’s 1992 paper where “Katherine Hayles made the mistake of re-expressing Irigaray’s thoughts in (comparatively) clear language. [...] And Dawkins adds: “It helps to have Sokal and Bricmont on hand to tell us the real reason why turbulent flow is a hard problem: the Navier–Stokes equations are difficult to solve.” There is at least one point when one should disagree with Dawkins: whom can it help to have Sokal’s libel on hand? Irigaray’s essay is a seven-page paper in the journal L’Arc, in a special issue dedicated to the pyschoanalyst Lacan; it is clearly not a treatise on the meaning of fluid mechanics, but a variation on Lacanian themes. The stake of the paper is the confrontation between the  symbolic order and reality [...] Hayles’s exposition of Irigaray’s theses is purely  provocative: she completely omits the psychoanalytic context of enunciation, and tries to give the most dramatic presentation to an “outrageous” thesis that would uprise engineers and hydraulicians. - Pierre Gilles Lemarié-Rieusset

# On Epistemological Relativism

So, what is the point in all of these criticisms? What could these authors possibly have done to become so threatening? In the chapter Intermezzo: Epistemic Relativism in the Philosophy of Science, the pair highlights their ultimate concern with these post modern texts - scientific relativism. Inspired by works by Thomas Kuhn and Paul Feyerabend, scientific relativism poses a major threat to understanding of science that Sokal and Bricmont present. But do they present a proper counter-critique?

Sokal and Bricmont insist that, in order to dismiss solipsism and radical skepticism, and reduce all knowledge into nothing, that one must base their beliefs on something material, which they point to sensory processes.

> "The answer, of course, is that we have no proof; it is simply a perfectly reasonable hypothesis. The most natural way to explain the persistence of our sensations (in particular, the unpleasant ones) is to suppose that they are caused by agents outside our consciousness. "  - Alan Sokal and Jean Bricmont

However, this kind of thinking is fundamentally flawed.

Humans possess many different kinds of bodies, and in the cases of disabled people, entirely different perceptions of everyday life. Does this mean that light doesn't exist because a blind person doesn't experience it, or that the alternate paranoid realities of a schizophrenic exist literally? Or does it mean that the lived experiences of both don't matter to science? Both have major implications for our existential relationship with the sciences.

Do Sokal and Bricmont seriously believe that disabled people should not be able to participate in science as a consequence? Should disabled people be inherently, irrefutably be subjects of science, with little capability to question it? This leads to a much larger problematic than simply one of civil rights - the scope of invisible disability and its impact on perception are inherently difficult to measure and therefore impact all sensory measurements in all people constantly in difficult to detect ways. Does Sokal and Bricmont expect these observations to be removed once a physical disability is detected? What method should be used to differentiate between these types of observations? There's a reason why science moved away from senses and moved towards abstractions read by instruments - something Sokal and Bricmont point out soon after.

> "We can then ask: To what extent are our senses reliable or not? To answer this question, we can compare sense impressions among themselves and vary certain parameters of our everyday experience. We can map out in this way, step by step, a practical rationality. When this is done systematically and with sufficient precision, science can begin."  - Alan Sokal and Jean Bricmont

Weird then how in the very next section they are talking about how the sensory approach is justified with abstract models and measurements, including mentioning the cliche of the magnetic moment of the electron which was not measured by our senses but rather powerful, precise instruments, translating many things not observable to our senses into a language as a carrier of this observation from the detector to the human observer. It's almost as if they believe the problem of our senses is completely gone now!

No, Sokal, no scientist has ever seen an electron; they calculated an electron was there based on using their senses to read instruments - instruments they may have miscalculated, results they may have misinterpreted, things they may have thought they seen. Sokal and Bricmont seem to think that the production of these methodologies and tools compensates for the inherent problem of interpretation - but this never ended when we moved the problem somewhere else they aren't consciously thinking about.

In fact, a large issue within science today is the overproduction of misinterpreted results, often because of social pressures like funding. We are humans reading interfaces suffering from human social factors and bodily differences. Sokal and Bricmont mostly hide behind the materialism of the thing they are measuring to avoid addressing the fact that, well, they're MEASURING something, but hiding from the problem doesn't make it go away. In some sense, science must be interpreted, and this is where the material finds itself separating from the knowledge. Because we are passing through senses, because we are thinking from a certain perspective constructed from those senses, we produce subjectivity.

I struggled to respond to this part because frankly, S&B's critique is very hard to parse here, mostly  because it's not clear how he unites sensory input  with the social and the measuring tools. In user  interface, the observer and these perspectives all  work together as a single machine to form a perspective.

You can think of this similar to playing a video game. Things that you already know to exist, like trees or enemies, are interpreted as representations on a screen. You organize the light from the screen in a certain way because of already pre-existing social expectations of the creator of the game. Similarly, measuring devices are created to answer questions that are already pre-existing to the observer - why else would they make such a device? And as a result, this causes the measurement to be relativistically contextualized around the social expectations of the scientist. This, along with their bodies, leads to different cutoff points with measurements, leading to different results. While the material outside is still there, the authority of a specific understanding of it, the Western scientific consensus, breaks down.

But why should we care about their feelings on epistemological relativism? What do they believe it to be its impact?

At the end of the chapter, they list three examples of how epistemological relativism damages the relationship that people have with science in multiple fields. But do these examples really hold up to scrutiny?

First, they start with criminology. They point out a case in 1996 where in Belgium, inept police work regarding a series of kidnap-murders lead to an internal investigation pressed by parliament. In the investigation, they found out that there was a miscommunication between the officers and the judge. They then proceed to quote Professor Yves Winkin on the question of truth.

> "I think that all the work of the commission is based on a sort of presupposition that there exists, not a truth, but the truth—which, if one presses hard enough, will finally come out.  However, anthropologically, there are only partial truths, shared by a larger or smaller number of people: a group, a family, a firm. There is no transcendent truth. Therefore, I don’t think that judge Doutrèwe or officer Lesage are hiding anything: both are telling their truth."  - Professor Yves Winkin 

Sokal and Bricmont seem confused about what the professor is actually saying about the incident. The professor is explaining that, to the judge and the officers, there are two understood truths about what happened - not what the material result of what happened. This is important information because it highlights both the understanding of the truth to both sides, as well as explaining why it happened the way it did. It seems that the pair seem confused about what role motives and awareness play into a criminal investigation.

The officer and the judge, despite Sokal and Bricmont's claim, DO actually occupy different universes. They may both speak Belgian, but they also speak a specialized language relative to their work, especially the judge. For example, a judge is likely not to know police codes, and an officer is likely unaware of the complex clerical work of the courtroom. It appears that to these two, a miscommunication and poor organization is the same thing as lying - but as all of us are aware, most situations are much more complex.

It is worth pointing out that criminal investigations are hardly scientific, are conducted by a privileged class of people in society and have a track record of having aggressive tactics, targeting vulnerable people, and activities bogged down by paperwork - All issues that result in questionable court decisions, a problem that has only grown more and more visible since the publication of this book, and is visible clearly in this case. So using this example doesn't seem very legitimate and instead reflects more about their belief of where the position of science belongs within politics. This isn't the only time they equate science to criminal investigations, either, which is honestly quite worrying.

The second example involves education. In this quote from a high school education book, it points out that facts can change over time and even be challenged based on new information. This seems like an odd take for scientists to question, but they clarify by saying the author clearly intended to mean "assertion" - a major nitpick if you ask me. They give the example that there are some facts that are simply unknown to someone at some time, but they are still "facts" that are unchanging - but this is not actually true.

For example, they give the example of Shakespheare's birthday not being known, but the existence of this date is still a fact regardless. What they fail to consider however is that if the date of Shakespeare's birthday is not known by anyone at the time of the writing, the fact of his birthday simply does not exist - the only fact that does exist is the knowledge of Shakespeare's birthday being known to exist, but not being known directly. The void for a fact to be placed does not imply the existence of the specific fact - the void might be produced by something else (for example, if Shakespeare was later discovered to be another author's alter ego, he would have no real birthday). This means that when new evidence is produced that confirms Shakespeare's birthday, the fact is, in fact, *changed* - it is no longer a void. And this cannot be merely dissolved into an assertion either, without dissolving ALL facts into assertions - because the fact is completely inaccessible before the discovery.

They then take this claim to absurd heights, claiming that the author is stating that the Earth revolved around the Sun only since Copernicus's time. This is not what the author is saying - the author is saying that facts are connected to collective knowledge, not a material reality, because they must be observed first - and this observation is what produces the relativity that Sokal and Bricmont despise so much. Of course, this kind of hyperbole is nothing new for Sokal and Bricmont.

The third claim has to deal with complex Indian politics. Possibly the most convincing argument against relativism presented, they highlight a case that Meera Nanda, an Indian biochemist, notes that an Indian politician, following a superstitious belief, demolished a slum near their office, and western social relativists justified it on the basis of having different perspectives. However, this seems more likely to be a case of western ignorance of complex social issues rather than a case of actual epistemological relativism. While its true that superstitious beliefs can lead to destruction of people's homes, what is ignored in this example is how the politician has power over these people's lives. Should the politician even have the power to demolish a slum in the first place? Why do Sokal, Bricmont and Nanda not highlight this problem themselves?

# God I hate this book so much please make it stop

Hey, you know what, let's stop talking about this book. How about we talk about the Sokal Affair itself. That was a huge embarrassment for Social Text, wasn't it?

Well, it seems that of course, the truth is always more complicated. In my first video on the Science Wars, I implied that Social Text was caught off guard, but after further research it seems that there is much more involved in the case. In fact, the editors seemed pretty skeptical of the initial draft.

They considered the paper a "little hokey", and pretty strange. They knew little about Sokal's background and were not sure of the intentions of the paper, but they didn't seem to be fooled by its content. Rather, they just found it unusual and outdated. Additionally, the obsession with footnotes made the paper look genuinely insecure to the editors. They read into the submission as an act of good faith, not something that was going to change the fields of philosophy or social science.

They asked him they would accept the article with two major edits - to remove the excessive footnotes, and trim the excessive philosophical speculation. However, Sokal wouldn't budge. He insisted the article published as is or not at all. The editors would label him as a "difficult, uncooperative author" for his trouble. Not exactly a ringing endorsement! 

Social Text was not interested in the paper as a peer reviewed piece, but rather as a possible contribution to the discussion of the "Science Wars". Ever since the publication of Higher Superstitions by Paul R. Gross and Norman Levitt, the journal was interested in measuring the impact of its publication and how scientists and critics were responding to it.

Interested in publishing an issue with these accounts, they decided to set up a special side issue about the Science Wars. Sokal was concerned, but he allowed it to go in print, with the editors under the impression that, despite being out of the field and the ideas being... well, out there, they might provide some contribution meaningful to the discussion.

Unfortunately, they were wrong. Within a month, the article was pulled. But the damage was already done. Ever since Sokal set up the stink, the field was de-legitimized in the eyes of many scientists. The affair has been used for years to oppress the discussion of minority and leftist politics and the impact of science on its boundaries, under the guise of "post modernism", and pursue over 25 years of truly unscientific, unprofessional critiques of these fields, fueled by a book that denies climate science, and yet crystallized by none other than Richard Dawkins and his book review in 1998. I think now is a better time than any to reclaim things back.

Perhaps we should leave with how the editors of Social Text, Bruce Robbins and Andrew Ross, end their response to the scandal:


> Why does science matter so much to us? Because its power, as a civil religion, as a social and political authority, affects our daily lives and the parlous condition of the natural world more than does any other domain of knowledge. Does it follow that nonscientists should have some say in the decision-making processes that define and shape the work of the professional scientific community? Some scientists (including Sokal, presumably) would say yes, and in some countries non-expert citizens do indeed participate in these processes. All hell breaks loose, however, when the following question is asked: Should nonexperts have anything to say about scientific methodology and epistemology? After centuries of scientific racism, scientific sexism, and scientific domination of nature, one might have thought this was a pertinent question to ask. - Bruce Robbins and Andrew Ross

References:
- Fashionable Nonsense, Alan Sokal and Jean Bricmont
- Chaosmosis, Félix Guattari
- The Navier-Stokes Problem in the 21st Century, Pierre Gilles Lemarié-Rieusset
- Mystery Science Theater - Lingua Franca, Bruce Robbins and Andrew Ross(http://linguafranca.mirror.theinfo.org/9607/mst.html)

Psychology references (Thank you Bunny!)
1. McCulloch, Pitts (1943). A logical calculus of the ideas immanent in nervous activity,
   Department of Psychiatry at the Illinois Neuropsychiatric Institute, 
   University of Illinois, College of  Medicine,
   University of Chicago, Chicago, U.S.A.
2. Rinkus (1996). A combinatorial neural network exhibiting episodic and semantic memory properties for spatio-temporal patterns 
   Boston University
3. Ferrier (2014). Toward a Universal Cortical Algorithm: Examining Hierarchical Temporal Memory in Light of Frontal Cortical Function,
   Department of Cognitive Linguistic and Psychological Sciences,
   Brown University
4. Reimann, et al. (2017). Cliques of Neurons Bound into Cavities Provide a Missing Link between Structure and Function
5. Byrne (2015). Encoding Reality: Prediction-Assisted Cortical Learning Algorithm in Hierarchical Temporal Memory
6. Jones (2002). Thalamic circuitry and thalamocortical synchrony.
7. Homan, et al. (2019). Neural computations of threat in the aftermath of combat trauma
8. Varier, Kaiser, and Forsyth (2011). Establishing, versus maintaining, brain function: a neuro-computational model of cortical reorganisation after injury to the immature brain
9. Feinberg and Gonzalez (2012). APA Handbook of Research Methods in Psychology: Vol. 2. Research Designs, H. Cooper (Editor-in-Chief)
