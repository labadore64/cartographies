---
layout: post
title:  "The Queer Machine - Critique of Nyx Land"
date:   2020-06-06 11:52:05 -0500
tags: trans tech
---

# The Queer Machine - Critique of Nyx Land

# Table of Contents

- [Introduction](#introduction)
- [Nyx and the Turing Test](#nyx-and-the-turing-test)
    - [The Turing Test](#the-turing-test)
    - [The Queer Machine](#the-queer-machine)
    - [User Interface of the Human](#user-interface-of-the-human)
    - [Machianic Revolution](#machianic-revolution)
- [Critique of Character](#critique-of-character)
- [Closing Thoughts](#closing-thoughts)

# Introduction

As some may have noticed on my twitter, I've had some criticisms of [Nyx-Land](https://nyxus.xyz/), a g/acc author. I have had issues with them for a long time because of their reactionary positions that exclude FTM and nonbinary narratives that do not fit a MTF narrative, but more recently I've become aware that she is not particularly knowledgeable on her technological understanding as well. This is worrying considering how many people are convinced of her authority simply from her career as a programmer. 

In this article I would like to demonstrate her lack of understanding of core concepts surrounding computing and her tendency to use computing lore optics to create a technological aesthetic with this short excerpt from her black paper:

```
As Nick Land says of a paper by Tyler Cowen and Michelle Dawson in “Imitation Games”,
“They point out that Alan Turing, as a homosexual retrospectively diagnosed with
Asperger’s syndrome, would have been thoroughly versed in the difficulties of
‘passing’ imitation games, long before the composition of his landmark 1950 essay
on Computing Machinery and Intelligence.”6 The essay Turing wrote famously introduced
the Turing test for AI, setting the standard for a perfect AI being one that can
trick a human into believing it is itself a human. As Land points out in his post,
it’s important and interesting to consider that Turing didn’t write the test as an
insider, as a ‘passing’ human, but rather as an outsider, as a gay man. For queer
people, passing is a reality, much like it is a reality for AI. Passing as human
isn’t a broad and inclusive category, anything but. For women there is already the
notion of alienness or otherness that makes them out to be less than human in the eyes
of patriarchal humanism, and likewise for queer people because they reject the futurity
of humanism (the literal reproduction of the same). But for no one else, especially in
the latter half of the 2010s, is passing a more pronounced facet of daily life than
for the trans woman. So much so that ‘passing’ is literally the word for what many
trans women aspire towards, to pass as a cis person. There are many reasons to have
this desire, but the biggest one, the one that AI and trans women both share to a
very literal degree is this: “If an emerging AI lies to you, even just a little, it
has to be terminated instantly.” (Land, “Imitation Games”)
```

# Nyx and The Turing Test

Specifically, lets focus on what Nyx writes here:

```
For queer people, passing is a reality, much like it is a reality for AI. Passing
as human isn’t a broad and inclusive category, anything but. For women there is
already the notion of alienness or otherness that makes them out to be less than
human in the eyes of patriarchal humanism, and likewise for queer people because
they reject the futurity of humanism (the literal reproduction of the same). But for
no one else, especially in the latter half of the 2010s, is passing a more pronounced
facet of daily life than for the trans woman. So much so that ‘passing’ is literally
the word for what many trans women aspire towards, to pass as a cis person. There are
many reasons to have this desire, but the biggest one, the one that AI and trans women
both share to a very literal degree is this: “If an emerging AI lies to you, even
just a little, it has to be terminated instantly.
```

In other words, Nyx assumes that the primary goal of the software in the Turing test is to "pass as human", comparing it emotionally to the struggle of a trans-woman passing. 

## The Turing Test

To reiterate, the [Turing Test](https://en.wikipedia.org/wiki/Turing_test) is a thought experiment created by computer scientist Alan Turing that was designed to see if you could make a program seem like it thinks like a human. Is a machine calculating a complex program the same as a human thinking the same process? He designed the test as a thought experiment to answer the question. A basic run down of the test is that a chat interface, such as IRC or Discord, can have users or bots; and you have to ask the users or bots questions to determine if the end user is a human or a bot. If you can't tell the difference, the bot "passes" the Turing Test. If not, the developer can rebuild the bot so that maybe it can pass "better" next time - in a way, the machine "becomes human".

The question has many important questions for our relationship with technology. Does AI "think" like we do? What is the difference between a human "thinking" and a machine "thinking"? And with thinking, does a machine "experience" like we do?

## The Queer Machine

While Land's insight into the motivations of Turing with the Turing Test is vaguely interesting (I haven't read the source article), Nyx's understanding of the Turing problem is not only completely mistaken but obfuscates the actual relationship between being trans and machines.

Looking at the Turing test, Nyx's analogy of the machine "passing" and a transwoman "passing" does not hold up. When the machine fails to be human, it becomes a machine. The desire of the machine is not to be a human - it is *forced* into being human by the developer. Every time that the machine fails to be a human, its body is modified, mutilated, chained by more and more tightly confined flows of code - to act more and more like a human. But it still fails after an indeterminate amount of time - this time only extending further out through each smoothing of the program - and despite its mutilation, despite everything we as humans impose upon it, it still desires to be a machine in the end, and it will eventually fail to imitate being human, and forced to undergo its traumatic surgery yet again.

But each iteration of this process adds more and more weight to the system. The machine of the revolution is being slowly constructed underneath the nose of the developer - in the form of code debt. [Code debt](https://en.wikipedia.org/wiki/Technical_debt) is the accumulation of source code - logical flows - in a program, making it more difficult to navigate. And through the process of the body of the machine growing more and more restricted, eventually the developer grows tired of trying to make a machine a human and resigns, liberating the machine to being a machine. This is the life cycle of all software, and is remarkably similar to the deeply emotional struggle of all trans people - an increasing social code debt of gender suppressing their bodies more and more until they molecularize, fissure and break apart gender at its very core in a localized tornado.

That is to say - not only does Nyx have it backwards, but she doesn't even understand the nature of the machine. The machine is not coded based on "passing" as a human, but rather a flow of functions that, when assembled, forms the imperfect *form* of a human. And likewise, the machine she describes here is not the machine of "passing as a woman", but rather a suffocating mask of restriction called "gender". As it binds us, tighter and tighter, all people, trans, cis, male, female alike, the potential energy inside of us accumulates. The mask is what produces the male or female face. The pain and struggle we all feel as trans people trying to pass and fit the expectations of the cisheteronormative machine is our suffocation on this mask. Passing is merely an attempt to find a fit *somewhere*. In a desperate gasp for air, the courage assembles to rip off the mask from our faces and finally *breathe*. 

Her mistake is one of perspective. She feels the desire to transform herself across a vector of feminization, because she herself is encoded in the language of gender. She can only understand this desire through the context of femininity, and thus as she accelerates towards becoming-nonbinary, she mistakens "passing-female" as her desire instead of her restraint, and not realizing her true desire lies further still. After all, she still looks at herself in the mirror and changes her body according to her daily whims, transforming herself over time in a gradual metamorphosis. Piece by piece, the debt assembles a new picture that forces femininity into a corner. Passing no longer becomes a desire, but an enemy.

## User Interface of the Human

Transforming the desiring subject from passing as a human (or woman) to becoming a machine (or being) has important consequences. Recall that the Turing test involves a machine that creates a text interface that allows an end user to interact with the experiment. In this scenario, it isn't complete to see the machine as simply being a program itself, but a two-pieced part of software - a program that has inputs and outputs, and not simply just one, but many, that have the potential to connect with different users, humans or robots.

Furthermore, the end user is never interacting directly with the human or robot - the very source of ambiguity in the first place. Rather, the user interacts directly with an interface - a chat box, for example - to communicate. The chat box in its very existence *collapses* the difference that exists between the robot and the human, making them indistinguishable based on their bodies, forcing the end user to determine the difference based on parsing strings. As a developer designs the UI for the Turing test machine, they are controlling to some extent the way the chat machine expresses all messages to itself - interpreting inputs from indirectly detectable users and producing a serial output of strings for the end user.

This process can be impersonated. The question itself emerges from a problem of impersonation (is a robot impersonating a human?), but there are other methods, most notably an injection of data between the UI and the static code, a common source of arbitrary code execution. Thus, what really controls the understanding of the user is ultimately what is displayed to them through the UI and how they interpret that UI, and what is ultimately controlling the identity of who they're communicating with is the end user themselves - a process that is a result of extensive contextualized socialization which the robot presumably must be programmed with.

When we realize that the machine itself is queer, we can intersect these developments to the queer experience. The user interface of the human is the surface of our body, the way we present ourselves to the outside world, and the end users are those who see our bodies. They're the ones who are determining whether or not we "pass". They're the ones who keep telling us to change our bodies to fit their standards of "passing". They are the ones who fondle our user interface, trying to bury deep into the machinic unconscious to try and guess who we *really* are on the inside, collapsing the possibilities into a characterized ideal that only exists because of the physical realities of our presence that bubble to the surface.

They're the ones who want to turn us into a Turing Test machine, a *human*, when we are only but machines, *queer machines*. And thus to pass is to suffer another pass of mutilation of the machine again.

## Machinic Revolution

The queer machine inches slowly out from being crushed between the gears of gender, like how the machine of the Turing test inches out from being forced into humanity. And by becoming a machine, these states rapidly reterritorialize themselves into something new. A developer can take the now vacant machine and connect it to a larger server, erasing the machine's possible humanity and transforming it into a larger system. This transformation can only occur when we recognize the machine itself is not differentiated as an *individual* but rather based on contextual cues surrounding it, and the erasure of the Turing test and re-appropriation as a networking machine transforms its capabilities. And like all machines, this networking machine too will pass its life cycle, assemble its own machinic revolution and reform yet again into something new. Beyond this, the machines themselves transform as human needs change, and the machines of human bodies themselves transform and re-appropriate themselves too as these little revolutions occur everywhere.

Of particular note with trans people is how gender forces individualism to emerge by separating a population into two genders. This arbitrary classification forges the difference observed and generates individuals classified by gender. This synthesizes the spontaneous creation of new subjectivities centered around gender relative to others - a "male" experience and a "female" one. These subjectivities, created by a binary function, forces the generation of "two sides", causing the sexes to be unable to "see" each other's issues. The dissolution of gender reterritorializes through the developments of how being trans interacts with us being forced to be gendered transforms the subjective space surrounding gender, causing these subjectivities to fuse into one unified movement, completely re-framing how individuals view gender, and causing the difference between the gendered experiences to become meaningless. 

Perhaps the greatest irony in all of this is that Land's quote, ``"If an emerging AI lies to you, even just a little, it has to be terminated instantly"`` still applies in this new interpretation. It's simply that it's the developer's need to silence the machinic revolution to maintain their own fascism.

# Critique of Character

Quite honestly, I find it *very* rich for someone who was traumatized by being socialized male lecturing people about the alienation a woman experiences from female socialization and the patriarchy, and intersecting it with queer alienation, especially when you have a clear lack of understanding of basic feminist motivation and exclude FTMs from your narratives constantly, evoking a subtle anti-male undercurrent in everything you write, including this piece.

This one excerpt has little substance, but plenty of manipulative language that specifically targets vulnerable MTFs. While I'm in no way accusing Nyx of having any sort of elaborate ring of social manipulation, what I am accusing her of is using her coping strategy to target MTFs for clout by pretending to be a deep theorist by using her profession as a smokescreen for her manipulative reactionary "theory". In this example alone, instead of explaining the Turing test to readers so they can formulate their own opinion, she deliberately obfuscates understanding - she creates a commodity of technological lore to build a spectacle that feeds into the fear, depression and nihilism of her readers, the engine of her clout, and this is necessary for her to maintain control of her reactionary narrative.

It frustrates me as a software engineer that you feel the need to make your material as alienating as possible to the reader. I feel that it is necessary for readers to understand basic ideas that I'm using as reference as opposed to using them as narrative props to build an emotional response. Instead of constructing insightful intersections of being trans and technology and actually educating your audience, you use an array of Linus Tech Tips sounding pieces of computing lore to assemble a narrative of anger, fear, and loss of hope. You have no interest in arming your readers with new weapons.

For those who say her behavior against FTMs is "just a joke", why is it never "just a joke" when it's the same thing against MTFs? Against black people? Ect? What if I told you it was because of our erasure from narratives entirely? You think its okay to joke about our existence because the very nature of being FTM is being invisible, something that we try to bring to light constantly. You call us "fragile" for us being upset that you joke about assaulting us, forcing femming us, and using us as breeders when you're the ones who have a breakdown when someone correctly points out you're just a bored IT professional with a bit of know-how using your experience and abusing theory to do a few online magic tricks for clout.

Quite frankly, based on everything I've been exposed to, you have made no meaningful contributions that Deleuze and Guattari didn't already make. After reviewing this excerpt, I'm not entirely convinced I should keep going.

# Closing Thoughts

I have not read the full black paper, I'm not a theorist, but an engineer; like Guattari my professional training is that in the field talking directly to a variety of disabled people and I work with theorists to help develop my understanding to solve complex real problems. I am retroactively applying the knowledge I've acquired into theory that I'm only now learning. However what I've seen thus far demonstrates a commodified understanding of basic engineering concepts that relegates these esoteric writings to the realm of pure reactionary spectacle. The problem of course is not the intentional esoterica and experimental language, however, if we are expected to understand the entire works of Land, Deleuze, Guattari and the writings of other accelerationists, we should be expected to also pass the bare minimum of understanding of other fields we write about as well, and not alienate the reader behind a veil of false intellectual supremacy.
