---
layout: post
title:  "An Introduction"
date:   2020-02-28 00:35:05 -0500
tags: personal
---
# ENTER: blog
This is the personal blog and website of punishedfelix. I intend to use this site to give a home to my longer musings. Twitter is simply too short of a media, but this site offers a place for ideas to set stage.

Thanks for reading.

[logo]: ../../../images/icon.png "Watching..."
