---
layout: post
title:  "Game Development and Accessibility"
date:   2020-02-29 01:44:05 -0500
tags: needs tech game
---
# Game Development and Accessibility
*Note: This article can also be read [ [offsite](https://gitlab.com/labadore64/maxlib-hub/-/blob/roger-docs/docs/theory/accessibility.md) ].*

## The Hierarchy of UI
User Interfaces (UI) are abstract interfaces that allows users to interact with the internals of a program. Both inputs and outputs are part of the UI structure.

In both proprietary and structurally-bloated software, the UI imposes a hierarchy between the developer and the end user, with the developer's design choices dominating the end user's experience. This hierarchy is well known and exploited by many ends in the industry, such as with the concept of Dark Design - design patterns that exist to direct the flow of applications towards or away certain behaviors.

With the disabled, the hierarchy is even more fundamental, and interferes directly with a disabled person's ability to interface with the world in many cases. The end user is subject to all the design choices of the software owner, and since the needs of disabled people are so specialized, they are often neglected entirely. While legal requirements do exist, they are not readily followed and the conflict between labor laws and disability laws usually results in most cases not being pursued. These problems are even more amplified when we are considering technology designed to help basic everyday functioning, such as mobility devices, GPS devices and phone accessibility.

In order for UIs to truly be fully accessible, the input and output channels must be abstract and be able to be completely reconstructed based on the needs of both the end user and the processor.

## Individual Responsibility
Individual responsibility may seem like an obvious point of failure for implementation of accessibility on the scale of the video game market. After all, if everyone took on the moral responsibility of taking on accessibility, wouldn't every game be inherently accessible?

However, this expectation is inherently flawed in its very design. 

Disability is not something that can be easily generalized into a single concept - each disability is unique in some way or another, and everyone's struggle with using technology reflects this. It is true that some generalizations can be made, but not every use case can be predicted, leading to most use cases being ignored for a few suboptimal ones based around accessibility standards framed around a few disability archetypes.

In addition, individual responsibility distributes accessibility work across individual teams, meaning that if a team fails to meet accessibility requirements, their entire product fails accessibility. This requires training every team with members trained in accessibility. This approach also means that only a few disabled people can test the usability of the software, representing a tiny fraction of actual needs. Furthermore, simply following standards does not give developer an understanding of why these standards exist or what material conditions they are trying to adapt for.

The results of this can be clearly be seen by the lack of implementation of accessibility in video games across the industry. There are simply too many factors interfering with individual studios reliably implementing even basic accessibility into their games outside of sheer happenstance. Even when games try to implement accessibility, they often miss the mark with specific issues that can't be readily fixed without developer input. This evokes the hierarchy of UI in the real material experiences of disabled people with the very software they use.

## Regulation and Implementation Issues
Currently, accessibility is managed by standards created by the government, such as the American with Disabilities Act (ADA), and technical institutions, such as WC3. In the last 30 years of both the ADA and the internet's proliferation, these standards have failed to actually implement accessibility in a real world setting reliably and are very limited.

The idea of accessibility standards is an extension of the Social Model of Disability, which models disability as a set of needs that should be socially accommodated for by recognizing certain disabilities and implementing solutions for them. However, this model fails because it requires the creation of disability archetypes, which is what standards are constructed around.

For example, blind accessibility standards are often about implementing audio cues or support for screen readers, however do not encourage necessarily the needs of the deaf-blind, who are only picked up by circumstance and completely miss audio cues, or the visually impaired, who would benefit from more than an interface designed for completely blind people. In addition, instead of understanding UI as an input-output interface that allows the application to interact with the user, standards atomize software design into a series of thoughtless steps. This leads to developers not truly understanding the needs of the disabled, by seeing them both as archetypes and standards rather than real material conditions to solve.

While this understanding is necessary to implement accessibility in an appropriate way per-project, it is a very technical understand that requires working directly with different kinds of disabled people, an intimate understanding of the project, and years of coding experience. These resources simply aren't available to most teams in this combination. 

Thus, accessibility is forced into two possible corners - either good but closed accessibility in high profile projects, and minimal, legally passing accessibility almost everywhere else that has a risk of getting sued. Anywhere else and accessibility is unpredictable and more a consequence of circumstance than design.

## Labor and Market Concerns
As stated previously, in order to have each team properly implement accessibility, they must have at least one member trained in implementing accessibility regulations, and preferably disabled testers who can ensure quality control over accessibility. However, this is simply not something viable in current labor conditions. Both the Indie and AAA game industries are struggling with serious labor violations, such as forced crunch time, sadistic labor practices, rampant backpay issues, among other issues. To put it mildly, outside of the largest studios, there is little room for accessibility unless a member of the team is specifically trained for it and decides to implement it into their design.

This issue is somewhat mitigated by the existence of accessibility contracting services where independent contractors will help implement and test accessibility. While not particularly common in the video game industry because of lack of accessibility regulation, this practice is quite common in other software markets. However, this service can be prohibitively expensive to smaller developer studios. In addition, because each implementation is individualized, it atomizes the accessibility further and encourages inconsistent implementation across contractors.

To mitigate the problem of smaller studios being unable to implement accessibility into their games, industry leading engines such as Epic Game's Unreal Engine 4 (UE4) are integrating accessibility into their engines. However, because this code is closed and under royalty contract, this obligates devs to use engines like UE4 that integrate accessibility. This forces capital in the direction of already established game engines. By eventually exploiting progression in both accessibility developments and legal protections for the disabled, the requirement of accessible gaming uses accessibility as a legal means to suffocate competition, thus enabling a market monopoly with currently existing engines. 

Because accessibility is privatized in this manner, multiple versions of accessibility exist where the end user is still subject to the UI hierarchy, and users must pick between implementations that are more suitable for them. This cuts off disabled end users. A great example of this effect in another tech market would be smart phones - because Apple invests so much in superior, heavily integrated accessibility, disabled people have to pick between a phone that they can use and the benefits that Android provides.

Furthermore, over time, as the market is seized by these private forces and the competition settles, the knowledge of the accessibility is locked inside these private organizations and codebases. Thus, as video games eventually outpace these engines, the knowledge of the accessibility structure embedded in these engines is lost. In the 90's, Windows integrated a intuitive and easy-to-implement accessibility engine into their Win32Forms applications. However, as developers started to build their own UIs and use of web browsers for applications proliferated, the accessibility knowledge was lost and still is locked inside Microsoft's private code base today.
