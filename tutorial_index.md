---
title: /tutorial
layout: page
permalink: /tutorial/index
---
# /tutorial
Do you want to set up your own Jekyll static-generated site like this one? 
To create a Jekyll static-generated website, you will need the following:

* Git
* Ruby
* A Gitlab account


Follow the instructions for setting up Git and Ruby your operating system here:

* [Windows](windows)
* [Linux](linux)


Then follow these instructions on the rest of setup:
* [Gitlab](gitlab)
* [Building your Site](building)
* [Themes](themes)


External resources:
* [Git Reference](https://git-scm.com/doc)
* [Jekyll](https://jekyllrb.com/)
* [Gitlab pages](https://docs.gitlab.com/ee/user/project/pages/)
* [Markdown Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)


All parts can be managed through the command line outside of setting up the GitLab account and should be fully accessible.
