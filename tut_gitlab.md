---
title: /tutorial
layout: page
permalink: /tutorial/gitlab
---
# Gitlab
First, make an account on [Gitlab](https://gitlab.com/).

After creating your account, make sure you set up SSH keys so that you can push to your repository without using your password. You can read about how to do that by following [this guide](https://docs.gitlab.com/ee/ssh/README.html#generating-a-new-ssh-key-pair).

## Create a template project
On the Gitlab homepage, under the plus sign, you can create a new project. Select "Create New Project" and then select Create from Template. Select the ``Pages/Jekyll`` template, and fork the repository to a new project.

*Note: The file ``.gitlab-ci.yml`` is outdated on the default template. Use the version provided [here](https://gitlab.com/labadore64/cartographies/-/blob/master/.gitlab-ci.yml) to prevent deployment errors.*

## Clone your repository
To clone your repository, open the terminal and run the following git command:
```
git clone [clone address]
```

The "clone address" is the SSH clone string given under the "clone" button on the base of your project's repository.

Executing this command will download all of the repository's files into a directory in the current terminal's running directory.
## Push your first update
After modifying your website to satisfaction, you can push your updates to the repository. To do this, execute the following:

```
git stage *
git commit -m "[Insert your git commit message here]"
git push
```

If you are on the master branch of your repository, it will push the changes directly to master.

NOTE: You should make sure that ``Gemfile.lock`` is in your ``.gitignore`` file. This file is created on your computer when testing and can conflict with deployment.

## Test your page
To access your page, look under your Project's Settings -> pages. Under ``Your pages are served under``, you should have a direct link to your website.

