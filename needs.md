---
title: /needs
layout: needs
permalink: /needs
---
# /needs
Texts on the **emancipatory** power of **needs**.
