---
title: /theory
layout: game
permalink: /theory
---
# /theory
Texts on **theoretical developments** in an unsound world.