---
notes_title: "2020-10-20 - Youtube and Free Will, Mice and Accessibility"
layout: note_single
date: 2020-10-20 01:00:00 -0500
---

# Youtube and Free Will

I haven't gone into a lot about Youtube specifically but the way that the algorithm and UI is designed work together to slowly reduce the choices of the end users and have measurable impacts on real world decision making of users. For several months now Youtube has switched to an algorithm that makes searching for certain videos very difficult because it usually has sponsored/large channels first, making finding specific old videos, especially source videos difficult. This narrows what people have access to. 

The feature the commenter is talking about here ["Youtube has decided that I want to watch a video about oil rigs and so i shall do as my overlord says"] is a feature of the algorithm to find videos that appear to be popular and promote them through the "Recommended for you" slot. This leads to a lot of seemingly unrelated content becoming popular, and because of it being frequently shared, can result in cultural influence through memes. A recent example is how a random diving safety video about delta pressure created a meme caused by a short clip of it going viral.

More insidiously, the process also causes certain narratives to persist in user's algorithms over time. Because youtube is integrated with android phones, this encourages accounts to be linked across a phone, making users possibly unaware they are logged in and their watch history influencing both their own interactions with the algorithm and the algorithm at large. This causes users to become more and more alienated from others outside as they are exposed to more content that promotes narratives that alienate them from friends and family, ranging from extremist political causes to cult like religious views. These proceses are used at large to create movements for larger social apparatus, such as recruiting people for ideologies or fueling certain industries.

I've been watching a lot of factory safety and accident analysis videos lately and something I notice in the comment section is how the UI also encourages an alienated way that we interact with the content. Many viewers were using this as an alternative to more dramatized depictions of industry accidents instead of viewing them for safety or informational purposes. The way that we interact with youtube causes us to think differently about the content we're consuming and reproduce it in our own minds in a specific way. I think this is particularly applicable in philosophy or media analysis channels, which have a tendency to associate philosophical ideas with plot elements in an attempt to engage users with the theory but often end up either misrepresenting the theory and/or acting as a substitute for engaging with the theory beyond the videos on YouTube

(there are certain channels i can think of but im not gonna list any names...)

whats interesting about that comment is that it not only implies that its changing the user's actions but also their actual desires in the future - and this is true, by encouraging a user to actively look up other accident videos. This is also replicated in the other cases including the highly politically relevant ones.

# Mice and Accessibility

I'm stupid. It turns out that my own fascist design principles got in the way of me desinging an important accessibility feature, and I want to talk a little bit about it before I submit a more formalized writing on it.

Basically a mouse is a device that allows you to navigate a 2D topology by moving the device across an x/y axis. Mice are equipped with several standard buttons (left, middle and right) and a scroll wheel to allow the user to pan the camera up and down along the scrollwheel, in typical implementation. The mouse has a unique ability to combine its 2D navigation with button movements to create mouse-based gestures, which can be used to traverse even 3D space with relative ease and control. In 3D modelling applications, its not unusual to have a mouse use the movement with different buttons being pressed (none, left, middle and right) to both control the tools and the camera, allowing for a highly condensed and powerful navigation tool.

Mice-based interfaces are particularly useful for certain kinds of motor disabilities, because many disability peripherals are designed around the basic mouse driver used in these applications. Keyboard input can be difficult at times for these kinds of disabilities, thus having the ability to move a short distance with an input device and press a button enables high levels of accessibility. It also gives another logical means to navigate the UI, allowing those with mental disabilities to have more opportunities to mix-and-match UI elements to find their way around.

Having the mouse not interfere with the keyboard is very important. The keyboard is assumed to always be active as a backup interface at all times with my game The Sequence, but the mouse offers alternative input. As such it must not override mouse or controller controls, but be used as an alternative input. Additionally it will have an option to be turned off since some users might find it an interference.

The reason why I was neglectful of this accessibility possibility was mostly because of my background in focusing on blind accessibility, or more recently my own migraine accessibility. Touch typing is more effective for me, while it can be seriously an issue for other users.

Another nice feature is that because of the extensive accessibility for blindness/visual impairment in the core game engine, it can use components from that to allow for text-to-speech to be usable with the mouse, enabling a higher level of accessibility. Since it isn't just ripped from a forms application core, it is customized much more closely to the UI, having a more flexible design.

Something worth pointing out with mice is that their design is reflective of forms applications, a major problematic UI design pattern persistent through the industry. This can be seen reflected in the scroll wheel, which only has vertical scrolling - designed to facilitate movement up and down a page, which can of course be filled with forms.

I fucking hate forms. I swear to god. This cancer is everywhere.