---
title: /bio
layout: page
permalink: /bio
uiq: true
---
# /bio

I'm just a random schizospec programmer on the internet who likes French psychoanalyist Félix Guattari.

My special powers include:

1. I can't pronounce his name correctly even if instructed repeatedly.
2. I can make some mild sprite and music changes in ROM hacking, as well as run a YouTube channel.
3. I am very good at reading books extremely slowly.

For those who are wondering, God commanded me to do everything I've created on this website through radio communication with angels. Make of that what you will.

No, I do not speak French (yet).
