---
title: /nodes
layout: page
permalink: /nodes
uiq: true
---
# /nodes
Various *interesting places* on the network.

# Affiliates

* [Adequate Ideas](https://otto-vogel.github.io/) - A blog by a friend.

# Interesting Sites

* [Chimeres](www.revue-chimeres.fr) - Guattarian Journal. Full of resources, in French.
* [Chaosmosemedia](https://chaosmosemedia.net/en/) - One of the active Guattarian nodes on the internet.
* [Machinic Unconscious Happy Hour](https://soundcloud.com/podcast-co-coopercherry) - Casual theory podcast run by my friend Cooper Cherry and featuring translator Taylor Atkins, discussing various theorists and media.
* [Polymorphous Space](https://anarchy.translocal.jp/) - Website of free radio activist Testuo Kogawa, over 20 years of material. Also preserved on Internet Archive.
* [Terminal Beach](https://cargocollective.com/terminal-beach) - Film projects by Silvia Maglioni and Graeme Thomson. 
* [marconius](https://marconius.com/) - Incredibly helpful website for web accessibility resources, such as [accessibility standards overview](https://marconius.com/a11y/) and [useful online resources](https://marconius.com/a11yLinks/)
